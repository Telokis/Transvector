#include "gtest/gtest.h"
#include "Transvector/Vector2.hh"
#include "Transvector/Vector.hh"
#include "Transvector/VectorCast.hh"

TEST(Casts_VectorCast, cross_generic)
{
  Transvector::Vector2<int> vec2_int(24, 35);

  const auto vec3_double = Transvector::GenericVectorCast<double, 3>(vec2_int);
  ASSERT_TRUE(vec3_double.x() == 24.0
              && vec3_double.y() == 35.0
              && vec3_double.z() == 0);

  const auto vec4_float = Transvector::GenericVectorCast<float, 4>(vec2_int);
  ASSERT_TRUE(vec4_float.at(0) == 24.0f
              && vec4_float.at(1) == 35.0f
              && vec4_float.at(2) == 0.0f
              && vec4_float.at(3) == 0.0f);
}

TEST(Casts_VectorCast, cross_generic_nodimension)
{
  Transvector::Vector2<int> vec2_int(24, 35);

  const auto vec2_double = Transvector::GenericVectorCast<double>(vec2_int);
  ASSERT_TRUE(vec2_double.x() == 24.0
              && vec2_double.y() == 35.0);

  const auto vec2_float = Transvector::GenericVectorCast<float>(vec2_int);
  ASSERT_TRUE(vec2_float.x() == 24.0f
              && vec2_float.y() == 35.0f);
}

TEST(Casts_VectorCast, cross_direct)
{
  Transvector::Vector2<int> vec2_int(24, 35);

  const auto vec3_double = Transvector::VectorCast<Transvector::Vector3<double>>(vec2_int);
  ASSERT_TRUE(vec3_double.x() == 24.0
              && vec3_double.y() == 35.0
              && vec3_double.z() == 0);

  const auto vec4_float = Transvector::VectorCast<Transvector::GenericVector<float, 4>>(vec2_int);
  ASSERT_TRUE(vec4_float.at(0) == 24.0f
              && vec4_float.at(1) == 35.0f
              && vec4_float.at(2) == 0.0f
              && vec4_float.at(3) == 0.0f);
}

namespace
{
  template <class T>
  struct FakeVec
  {
    FakeVec(T x, T y, T z)
      : _x(x), _y(y), _z(z)
    {
    }
    FakeVec() = default;

    using ValueType = T;
    static const std::uint16_t dimension = 3;

    T& at(std::uint16_t i)
    {
      switch (i)
      {
        case 0:
          return _x;
        case 1:
          return _y;
        case 2:
          return _z;
        default:
          throw std::logic_error("Out of bounds");
      }
    }

    const T& at(std::uint16_t i) const
    {
      switch (i)
      {
        case 0:
          return _x;
        case 1:
          return _y;
        case 2:
          return _z;
        default:
          throw std::logic_error("Out of bounds");
      }
    }

    T _x;
    T _y;
    T _z;
  };
}

TEST(Casts_VectorCast, from_custom_compliant_generic_nodimension)
{
  auto toConvert = FakeVec<int>(37, -20, 14);

  auto result_int = Transvector::GenericVectorCast<int>(toConvert);
  ASSERT_TRUE(result_int.x() == 37
              && result_int.y() == -20
              && result_int.z() == 14);

  auto result_double = Transvector::GenericVectorCast<double>(toConvert);
  ASSERT_TRUE(result_double.x() == 37.0
              && result_double.y() == -20.0
              && result_double.z() == 14.0);

  auto result_float = Transvector::GenericVectorCast<float>(toConvert);
  ASSERT_TRUE(result_float.at(0) == 37.0f
              && result_float.at(1) == -20.0f
              && result_float.at(2) == 14.0f);
}

TEST(Casts_VectorCast, from_custom_compliant_generic_nodimension_notype)
{
  auto toConvert = FakeVec<int>(37, -20, 14);

  auto result_int1 = Transvector::GenericVectorCast(toConvert);
  ASSERT_TRUE(result_int1.x() == 37
              && result_int1.y() == -20
              && result_int1.z() == 14);

  auto result_int2 = Transvector::GenericVectorCast(toConvert);
  ASSERT_TRUE(result_int2.x() == 37
              && result_int2.y() == -20
              && result_int2.z() == 14);

  auto result_int3 = Transvector::GenericVectorCast(toConvert);
  ASSERT_TRUE(result_int3.at(0) == 37
              && result_int3.at(1) == -20
              && result_int3.at(2) == 14);
}

TEST(Casts_VectorCast, from_custom_compliant_generic)
{
  auto toConvert = FakeVec<int>(37, -20, 14);

  auto result_int = Transvector::GenericVectorCast<int, 2>(toConvert);
  ASSERT_TRUE(result_int.x() == 37 && result_int.y() == -20);

  auto result_double = Transvector::GenericVectorCast<double, 3>(toConvert);
  ASSERT_TRUE(result_double.x() == 37.0
              && result_double.y() == -20.0
              && result_double.z() == 14.0);

  auto result_float = Transvector::GenericVectorCast<float, 4>(toConvert);
  ASSERT_TRUE(result_float.at(0) == 37.0f
              && result_float.at(1) == -20.0f
              && result_float.at(2) == 14.0f
              && result_float.at(3) == 0.0f);
}

TEST(Casts_VectorCast, from_custom_compliant_direct)
{
  auto toConvert = FakeVec<int>(37, -20, 14);

  auto result_int = Transvector::VectorCast<Transvector::Vector2<int>>(toConvert);
  ASSERT_TRUE(result_int.x() == 37 && result_int.y() == -20);

  auto result_double = Transvector::VectorCast<Transvector::Vector3<double>>(toConvert);
  ASSERT_TRUE(result_double.x() == 37.0
              && result_double.y() == -20.0
              && result_double.z() == 14.0);

  auto result_float = Transvector::VectorCast<Transvector::GenericVector<float, 4>>(toConvert);
  ASSERT_TRUE(result_float.at(0) == 37.0f
              && result_float.at(1) == -20.0f
              && result_float.at(2) == 14.0f
              && result_float.at(3) == 0.0f);
}

TEST(Casts_VectorCast, to_custom_compliant)
{
  Transvector::Vector2<int> vec2_int(37, -20);
  auto result_int = Transvector::VectorCast<FakeVec<int>>(vec2_int);
  ASSERT_TRUE(result_int._x == 37 && result_int._y == -20 && result_int._z == 0);

  Transvector::Vector3<double> vec3_double(24.64, 35.18, 22.11);
  auto result_double = Transvector::VectorCast<FakeVec<double>>(vec3_double);
  ASSERT_TRUE(result_double._x == 24.64 && result_double._y == 35.18 && result_double._z == 22.11);

  Transvector::GenericVector<float, 4> vec4_float(24.64f, 35.18f, 3.4f, -8.01f);
  auto result_float = Transvector::VectorCast<FakeVec<float>>(vec4_float);
  ASSERT_TRUE(result_float._x == 24.64f && result_float._y == 35.18f && result_float._z == 3.4f);
}

namespace
{
  template <class T>
  struct FakeVec2
  {
    FakeVec2(T x, T y, T z)
      : _x(x), _y(y), _z(z)
    {
    }
    FakeVec2() = default;

    T _x;
    T _y;
    T _z;
  };
}

namespace Transvector
{
  template <class T>
  struct VectorTraits<FakeVec2<T>>
  {
    using                       VectorType  = FakeVec2<T>;
    using                       ValueType   = T;
    static const std::uint16_t  dimension   = 3;

    static ValueType& at(VectorType& t, std::uint16_t i)
    {
      switch (i)
      {
        case 0:
          return t._x;
        case 1:
          return t._y;
        case 2:
          return t._z;
        default:
          throw std::logic_error("Out of bounds");
      }
    }

    static const ValueType at(const VectorType& t, std::uint16_t i)
    {
      switch (i)
      {
        case 0:
          return t._x;
        case 1:
          return t._y;
        case 2:
          return t._z;
        default:
          throw std::logic_error("Out of bounds");
      }
    }
  };
}

TEST(Casts_VectorCast, from_custom_specialized_generic_nodimension)
{
  auto toConvert = FakeVec2<int>(37, -20, 14);

  auto result_int = Transvector::GenericVectorCast<int>(toConvert);
  ASSERT_TRUE(result_int.x() == 37
              && result_int.y() == -20
              && result_int.z() == 14);

  auto result_double = Transvector::GenericVectorCast<double>(toConvert);
  ASSERT_TRUE(result_double.x() == 37.0
              && result_double.y() == -20.0
              && result_double.z() == 14.0);

  auto result_float = Transvector::GenericVectorCast<float>(toConvert);
  ASSERT_TRUE(result_float.at(0) == 37.0f
              && result_float.at(1) == -20.0f
              && result_float.at(2) == 14.0f);
}

TEST(Casts_VectorCast, from_custom_specialized_generic_nodimension_notype)
{
  auto toConvert = FakeVec2<int>(37, -20, 14);

  auto result_int1 = Transvector::GenericVectorCast(toConvert);
  ASSERT_TRUE(result_int1.x() == 37
              && result_int1.y() == -20
              && result_int1.z() == 14);

  auto result_int2 = Transvector::GenericVectorCast(toConvert);
  ASSERT_TRUE(result_int2.x() == 37
              && result_int2.y() == -20
              && result_int2.z() == 14);

  auto result_int3 = Transvector::GenericVectorCast(toConvert);
  ASSERT_TRUE(result_int3.at(0) == 37
              && result_int3.at(1) == -20
              && result_int3.at(2) == 14);
}

TEST(Casts_VectorCast, from_custom_specialized_generic)
{
  auto toConvert = FakeVec2<int>(37, -20, 14);

  auto result_int = Transvector::GenericVectorCast<int, 2>(toConvert);
  ASSERT_TRUE(result_int.x() == 37 && result_int.y() == -20);

  auto result_double = Transvector::GenericVectorCast<double, 3>(toConvert);
  ASSERT_TRUE(result_double.x() == 37.0
              && result_double.y() == -20.0
              && result_double.z() == 14.0);

  auto result_float = Transvector::GenericVectorCast<float, 4>(toConvert);
  ASSERT_TRUE(result_float.at(0) == 37.0f
              && result_float.at(1) == -20.0f
              && result_float.at(2) == 14.0f
              && result_float.at(3) == 0.0f);
}

TEST(Casts_VectorCast, from_custom_specialized_direct)
{
  auto toConvert = FakeVec2<int>(37, -20, 14);

  auto result_int = Transvector::VectorCast<Transvector::Vector2<int>>(toConvert);
  ASSERT_TRUE(result_int.x() == 37 && result_int.y() == -20);

  auto result_double = Transvector::VectorCast<Transvector::Vector3<double>>(toConvert);
  ASSERT_TRUE(result_double.x() == 37.0
              && result_double.y() == -20.0
              && result_double.z() == 14.0);

  auto result_float = Transvector::VectorCast<Transvector::GenericVector<float, 4>>(toConvert);
  ASSERT_TRUE(result_float.at(0) == 37.0f
              && result_float.at(1) == -20.0f
              && result_float.at(2) == 14.0f
              && result_float.at(3) == 0.0f);
}

TEST(Casts_VectorCast, to_custom_specialized)
{
  Transvector::Vector2<int> vec2_int(37, -20);
  auto result_int = Transvector::VectorCast<FakeVec2<int>>(vec2_int);
  ASSERT_TRUE(result_int._x == 37 && result_int._y == -20 && result_int._z == 0);

  Transvector::Vector3<double> vec3_double(24.64, 35.18, 22.11);
  auto result_double = Transvector::VectorCast<FakeVec2<double>>(vec3_double);
  ASSERT_TRUE(result_double._x == 24.64 && result_double._y == 35.18 && result_double._z == 22.11);

  Transvector::GenericVector<float, 4> vec4_float(24.64f, 35.18f, 3.4f, -8.01f);
  auto result_float = Transvector::VectorCast<FakeVec2<float>>(vec4_float);
  ASSERT_TRUE(result_float._x == 24.64f && result_float._y == 35.18f && result_float._z == 3.4f);
}

TEST(Casts_VectorCast, transvector)
{
  FakeVec<int> vec2_int(37, -20, 14);
  auto result_int = Transvector::VectorCast<FakeVec2<int>>(vec2_int);
  ASSERT_TRUE(result_int._x == 37 && result_int._y == -20 && result_int._z == 14);

  FakeVec2<double> vec3_double(24.64, 35.18, 22.11);
  auto result_double = Transvector::VectorCast<FakeVec<double>>(vec3_double);
  ASSERT_TRUE(result_double._x == 24.64 && result_double._y == 35.18 && result_double._z == 22.11);

  FakeVec<float> vec4_float(24.64f, 35.18f, 3.4f);
  auto result_float = Transvector::VectorCast<FakeVec2<float>>(vec4_float);
  ASSERT_TRUE(result_float._x == 24.64f && result_float._y == 35.18f && result_float._z == 3.4f);
}

int main(int ac, char** av)
{
  testing::InitGoogleTest(&ac, av);
  return (RUN_ALL_TESTS());
}
