#include "gtest/gtest.h"
#include "Transvector/Vector3.hh"
#include <string>

TEST(Vector3_Construction, default)
{
  Transvector::Vector3<int> vec3_int;
  ASSERT_TRUE(vec3_int.x() == 0 && vec3_int.y() == 0 && vec3_int.z() == 0)
    << "Values should initialize to 0";

  Transvector::Vector3<double> vec3_double;
  ASSERT_TRUE(vec3_double.x() == 0.0 && vec3_double.y() == 0.0 && vec3_double.z() == 0.0)
    << "Values should initialize to 0.0";

  Transvector::Vector3<float> vec3_float;
  ASSERT_TRUE(vec3_float.x() == 0.0f && vec3_float.y() == 0.0f && vec3_float.z() == 0.0f)
    << "Values should initialize to 0.0f";
}

TEST(Vector3_Construction, values)
{
  Transvector::Vector3<int> vec3_int(34, -83, 42);
  ASSERT_TRUE(vec3_int.x() == 34 && vec3_int.y() == -83 && vec3_int.z() == 42)
    << "Values should be {x = 34, y = -83, z = 42}";

  Transvector::Vector3<double> vec3_double(114.351, -351.847, 318.566);
  ASSERT_TRUE(vec3_double.x() == 114.351 && vec3_double.y() == -351.847 && vec3_double.z() == 318.566)
    << "Values should be {x = 114.351, y = -351.847, z = 318.566}";

  Transvector::Vector3<float> vec3_float(954.333f, -833.015f, 995.21f);
  ASSERT_TRUE(vec3_float.x() == 954.333f && vec3_float.y() == -833.015f && vec3_float.z() == 995.21f)
    << "Values should be {x = 954.333f, y = -833.015f, z = 995.21f}";
}

TEST(Vector3_Construction, fill)
{
  Transvector::Vector3<int> vec3_int(38);
  ASSERT_TRUE(vec3_int.x() == 38 && vec3_int.y() == 38 && vec3_int.z() == 38)
    << "Values should be 38";

  Transvector::Vector3<double> vec3_double(43649.36);
  ASSERT_TRUE(vec3_double.x() == 43649.36 && vec3_double.y() == 43649.36 && vec3_double.z() == 43649.36)
    << "Values should be 43649.36";

  Transvector::Vector3<float> vec3_float(5184.33f);
  ASSERT_TRUE(vec3_float.x() == 5184.33f && vec3_float.y() == 5184.33f && vec3_float.z() == 5184.33f)
    << "Values should be 5184.33f";
}

template <class T>
bool implicit_initialization(const Transvector::Vector3<T>& vec, T expected_value)
{
  return vec.x() == expected_value && vec.y() == expected_value && vec.z() == expected_value;
}

TEST(Vector3_Construction, fill_implicit)
{
  bool res_int = implicit_initialization<int>(138, 138);
  ASSERT_TRUE(res_int)
    << "Fill constructor should be implicit";

  bool res_double = implicit_initialization<double>(58.35, 58.35);
  ASSERT_TRUE(res_double)
    << "Fill constructor should be implicit";

  bool res_float = implicit_initialization<float>(33.14f, 33.14f);
  ASSERT_TRUE(res_float)
    << "Fill constructor should be implicit";
}

int main(int ac, char** av)
{
  testing::InitGoogleTest(&ac, av);
  return (RUN_ALL_TESTS());
}
