#include "gtest/gtest.h"
#include "Transvector/Vector3.hh"
#include <string>

TEST(Vector3_SetGet, set_xyz)
{
  Transvector::Vector3<int> vec3_int(34, -83, 37);
  ASSERT_TRUE(vec3_int.x() == 34 && vec3_int.y() == -83 && vec3_int.z() == 37);
  vec3_int.x(1184).y(-334).z(996);
  ASSERT_TRUE(vec3_int.x() == 1184 && vec3_int.y() == -334 && vec3_int.z() == 996);

  Transvector::Vector3<double> vec3_double(114.351, -351.847, 369.77);
  ASSERT_TRUE(vec3_double.x() == 114.351 && vec3_double.y() == -351.847 && vec3_double.z() == 369.77);
  vec3_double.x(1184.1748).y(-334.33).z(99.7);
  ASSERT_TRUE(vec3_double.x() == 1184.1748 && vec3_double.y() == -334.33 && vec3_double.z() == 99.7);

  Transvector::Vector3<float> vec3_float(954.333f, -833.015f, 37.2f);
  ASSERT_TRUE(vec3_float.x() == 954.333f && vec3_float.y() == -833.015f && vec3_float.z() == 37.2f);
  vec3_float.x(345.3f).y(-334.88f).z(369.77f);
  ASSERT_TRUE(vec3_float.x() == 345.3f && vec3_float.y() == -334.88f && vec3_float.z() == 369.77f);
}

TEST(Vector3_SetGet, set_set)
{
  Transvector::Vector3<int> vec3_int(34, -83, 37);
  ASSERT_TRUE(vec3_int.x() == 34 && vec3_int.y() == -83 && vec3_int.z() == 37);
  vec3_int.set(1184, -334, 996);
  ASSERT_TRUE(vec3_int.x() == 1184 && vec3_int.y() == -334 && vec3_int.z() == 996);

  Transvector::Vector3<double> vec3_double(114.351, -351.847, 369.77);
  ASSERT_TRUE(vec3_double.x() == 114.351 && vec3_double.y() == -351.847 && vec3_double.z() == 369.77);
  vec3_double.set(1184.1748, -334.33, 99.7);
  ASSERT_TRUE(vec3_double.x() == 1184.1748 && vec3_double.y() == -334.33 && vec3_double.z() == 99.7);

  Transvector::Vector3<float> vec3_float(954.333f, -833.015f, 37.2f);
  ASSERT_TRUE(vec3_float.x() == 954.333f && vec3_float.y() == -833.015f && vec3_float.z() == 37.2f);
  vec3_float.set(345.3f, -334.88f, 369.77f);
  ASSERT_TRUE(vec3_float.x() == 345.3f && vec3_float.y() == -334.88f && vec3_float.z() == 369.77f);
}

TEST(Vector3_SetGet, get_xyz)
{
  const Transvector::Vector3<int> vec3_int(34, -83, 37);
  ASSERT_TRUE(vec3_int.x() == 34 && vec3_int.y() == -83 && vec3_int.z() == 37);

  const Transvector::Vector3<double> vec3_double(114.351, -351.847, 88.7);
  ASSERT_TRUE(vec3_double.x() == 114.351 && vec3_double.y() == -351.847 && vec3_double.z() == 88.7);

  const Transvector::Vector3<float> vec3_float(954.333f, -833.015f, 37.058f);
  ASSERT_TRUE(vec3_float.x() == 954.333f && vec3_float.y() == -833.015f && vec3_float.z() == 37.058f);
}

TEST(Vector3_SetGet, set_at)
{
  Transvector::Vector3<int> vec3_int(34, -83, 8478);
  vec3_int.at(0) = 1184;
  vec3_int.at(1) = -334;
  vec3_int.at(2) = 34;
  ASSERT_TRUE(vec3_int.x() == 1184 && vec3_int.y() == -334 && vec3_int.z() == 34);

  Transvector::Vector3<double> vec3_double(114.351, -351.847, 944667.77);
  vec3_double.at(0) = 1184.1748;
  vec3_double.at(1) = -334.33;
  vec3_double.at(2) = 34.33;
  ASSERT_TRUE(vec3_double.x() == 1184.1748 && vec3_double.y() == -334.33 && vec3_double.z() == 34.33);

  Transvector::Vector3<float> vec3_float(954.333f, -833.015f, -227.15f);
  vec3_float.at(0) = 345.3f;
  vec3_float.at(1) = -334.88f;
  vec3_float.at(2) = 34.88f;
  ASSERT_TRUE(vec3_float.x() == 345.3f && vec3_float.y() == -334.88f && vec3_float.z() == 34.88f);
}

TEST(Vector3_SetGet, get_at)
{
  const Transvector::Vector3<int> vec3_int(34, -83, 37);
  ASSERT_TRUE(vec3_int.at(0) == 34 && vec3_int.at(1) == -83 && vec3_int.at(2) == 37);

  const Transvector::Vector3<double> vec3_double(114.351, -351.847, 944667.77);
  ASSERT_TRUE(vec3_double.at(0) == 114.351 && vec3_double.at(1) == -351.847 && vec3_double.at(2) == 944667.77);

  const Transvector::Vector3<float> vec3_float(954.333f, -833.015f, -227.15f);
  ASSERT_TRUE(vec3_float.at(0) == 954.333f && vec3_float.at(1) == -833.015f && vec3_float.at(2) == -227.15f);
}

int main(int ac, char** av)
{
  testing::InitGoogleTest(&ac, av);
  return (RUN_ALL_TESTS());
}
