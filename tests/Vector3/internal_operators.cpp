#include "gtest/gtest.h"
#include "Transvector/Vector3.hh"
#include <string>

TEST(Vector3_InternalOperators, addition_vector)
{
  Transvector::Vector3<int> vec3_int(34, 35, 37);
  vec3_int += Transvector::Vector3<int>(37, -30, 25);
  ASSERT_TRUE(vec3_int.x() == 34 + 37 && vec3_int.y() == 35 + -30 && vec3_int.z() == 37 + 25)
    << "Values should add properly";

  Transvector::Vector3<double> vec3_double(34.64, 35.18, 84.22);
  vec3_double += Transvector::Vector3<double>(37.3375, -30.0015, 0.884);
  ASSERT_TRUE(vec3_double.x() == 34.64 + 37.3375 && vec3_double.y() == 35.18 + -30.0015 && vec3_double.z() == 84.22 + 0.884)
    << "Values should add properly";

  Transvector::Vector3<float> vec3_float(34.64f, 35.18f, 85.1f);
  vec3_float += Transvector::Vector3<float>(37.3375f, -30.0015f, 0.848f);
  ASSERT_TRUE(vec3_float.x() == 34.64f + 37.3375f && vec3_float.y() == 35.18f + -30.0015f && vec3_float.z() == 85.1f + 0.848f)
    << "Values should add properly";
}

TEST(Vector3_InternalOperators, addition_value)
{
  Transvector::Vector3<int> vec3_int(34, 35, 99);
  vec3_int += 17;
  ASSERT_TRUE(vec3_int.x() == 34 + 17 && vec3_int.y() == 35 + 17 && vec3_int.z() == 99 + 17)
    << "Values should add properly";

  Transvector::Vector3<double> vec3_double(34.64, 35.18, -24.3);
  vec3_double += -3780.3618;
  ASSERT_TRUE(vec3_double.x() == 34.64 + -3780.3618 && vec3_double.y() == 35.18 + -3780.3618 && vec3_double.z() == -24.3 + -3780.3618)
    << "Values should add properly";

  Transvector::Vector3<float> vec3_float(34.64f, 35.18f, 9.11f);
  vec3_float += 589.336f;
  ASSERT_TRUE(vec3_float.x() == 34.64f + 589.336f && vec3_float.y() == 35.18f + 589.336f && vec3_float.z() == 9.11f + 589.336f)
    << "Values should add properly";
}

TEST(Vector3_InternalOperators, subtraction_vector)
{
  Transvector::Vector3<int> vec3_int(34, 35, -37);
  vec3_int -= Transvector::Vector3<int>(37, -30, 22);
  ASSERT_TRUE(vec3_int.x() == 34 - 37 && vec3_int.y() == 35 - -30 && vec3_int.z() == -37 - 22)
    << "Values should subtract properly";

  Transvector::Vector3<double> vec3_double(34.64, 35.18, -18.66);
  vec3_double -= Transvector::Vector3<double>(37.3375, -30.0015, 3.22);
  ASSERT_TRUE(vec3_double.x() == 34.64 - 37.3375 && vec3_double.y() == 35.18 - -30.0015 && vec3_double.z() == -18.66 - 3.22)
    << "Values should subtract properly";

  Transvector::Vector3<float> vec3_float(34.64f, 35.18f, 9.1f);
  vec3_float -= Transvector::Vector3<float>(37.3375f, -30.0015f, 14.3f);
  ASSERT_TRUE(vec3_float.x() == 34.64f - 37.3375f && vec3_float.y() == 35.18f - -30.0015f && vec3_float.z() == 9.1f - 14.3f)
    << "Values should subtract properly";
}

TEST(Vector3_InternalOperators, subtraction_value)
{
  Transvector::Vector3<int> vec3_int(34, 35, -23);
  vec3_int -= 17;
  ASSERT_TRUE(vec3_int.x() == 34 - 17 && vec3_int.y() == 35 - 17 && vec3_int.z() == -23 - 17)
    << "Values should subtract properly";

  Transvector::Vector3<double> vec3_double(34.64, 35.18, 9.2);
  vec3_double -= -3780.3618;
  ASSERT_TRUE(vec3_double.x() == 34.64 - -3780.3618 && vec3_double.y() == 35.18 - -3780.3618 && vec3_double.z() == 9.2 - -3780.3618)
    << "Values should subtract properly";

  Transvector::Vector3<float> vec3_float(34.64f, 35.18f, 35.2f);
  vec3_float -= 589.336f;
  ASSERT_TRUE(vec3_float.x() == 34.64f - 589.336f && vec3_float.y() == 35.18f - 589.336f && vec3_float.z() == 35.2f - 589.336f)
    << "Values should subtract properly";
}

TEST(Vector3_InternalOperators, multiplication_vector)
{
  Transvector::Vector3<int> vec3_int(34, 35, -15);
  vec3_int *= Transvector::Vector3<int>(37, -30, 33);
  ASSERT_TRUE(vec3_int.x() == 34 * 37 && vec3_int.y() == 35 * -30 && vec3_int.z() == -15 * 33)
    << "Values should multiply properly";

  Transvector::Vector3<double> vec3_double(34.64, 35.18, 84.33);
  vec3_double *= Transvector::Vector3<double>(37.3375, -30.0015, 3.215);
  ASSERT_TRUE(vec3_double.x() == 34.64 * 37.3375 && vec3_double.y() == 35.18 * -30.0015 && vec3_double.z() == 84.33 * 3.215)
    << "Values should multiply properly";

  Transvector::Vector3<float> vec3_float(34.64f, 35.18f, 5.2f);
  vec3_float *= Transvector::Vector3<float>(37.3375f, -30.0015f, 3.2f);
  ASSERT_TRUE(vec3_float.x() == 34.64f * 37.3375f && vec3_float.y() == 35.18f * -30.0015f && vec3_float.z() == 5.2f * 3.2f)
    << "Values should multiply properly";
}

TEST(Vector3_InternalOperators, multiplication_value)
{
  Transvector::Vector3<int> vec3_int(34, 35, -25);
  vec3_int *= 17;
  ASSERT_TRUE(vec3_int.x() == 34 * 17 && vec3_int.y() == 35 * 17 && vec3_int.z() == -25 * 17)
    << "Values should multiply properly";

  Transvector::Vector3<double> vec3_double(34.64, 35.18, -25.32);
  vec3_double *= -3780.3618;
  ASSERT_TRUE(vec3_double.x() == 34.64 * -3780.3618 && vec3_double.y() == 35.18 * -3780.3618 && vec3_double.z() == -25.32 * -3780.3618)
    << "Values should multiply properly";

  Transvector::Vector3<float> vec3_float(34.64f, 35.18f, -20.13f);
  vec3_float *= 589.336f;
  ASSERT_TRUE(vec3_float.x() == 34.64f * 589.336f && vec3_float.y() == 35.18f * 589.336f && vec3_float.z() == -20.13f * 589.336f)
    << "Values should multiply properly";
}

TEST(Vector3_InternalOperators, division_vector)
{
  Transvector::Vector3<int> vec3_int(34, 35, -23);
  vec3_int /= Transvector::Vector3<int>(37, -30, 32);
  ASSERT_TRUE(vec3_int.x() == 34 / 37 && vec3_int.y() == 35 / -30 && vec3_int.z() == -23 / 32)
    << "Values should divide properly";

  Transvector::Vector3<double> vec3_double(34.64, 35.18, -2.1);
  vec3_double /= Transvector::Vector3<double>(37.3375, -30.0015, 1.2);
  ASSERT_TRUE(vec3_double.x() == 34.64 / 37.3375 && vec3_double.y() == 35.18 / -30.0015 && vec3_double.z() == -2.1 / 1.2)
    << "Values should divide properly";

  Transvector::Vector3<float> vec3_float(34.64f, 35.18f, -3.141f);
  vec3_float /= Transvector::Vector3<float>(37.3375f, -30.0015f, 3.5f);
  ASSERT_TRUE(vec3_float.x() == 34.64f / 37.3375f && vec3_float.y() == 35.18f / -30.0015f && vec3_float.z() == -3.141f / 3.5f)
    << "Values should divide properly";
}

TEST(Vector3_InternalOperators, division_value)
{
  Transvector::Vector3<int> vec3_int(34, 35, -2);
  vec3_int /= 17;
  ASSERT_TRUE(vec3_int.x() == 34 / 17 && vec3_int.y() == 35 / 17 && vec3_int.z() == -2 / 17)
    << "Values should divide properly";

  Transvector::Vector3<double> vec3_double(34.64, 35.18, -3.5);
  vec3_double /= -3780.3618;
  ASSERT_TRUE(vec3_double.x() == 34.64 / -3780.3618 && vec3_double.y() == 35.18 / -3780.3618 && vec3_double.z() == -3.5 / -3780.3618)
    << "Values should divide properly";

  Transvector::Vector3<float> vec3_float(34.64f, 35.18f, -23.4f);
  vec3_float /= 589.336f;
  ASSERT_TRUE(vec3_float.x() == 34.64f / 589.336f && vec3_float.y() == 35.18f / 589.336f && vec3_float.z() == -23.4f / 589.336f)
    << "Values should divide properly";
}

int main(int ac, char** av)
{
  testing::InitGoogleTest(&ac, av);
  return (RUN_ALL_TESTS());
}
