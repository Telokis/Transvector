#include "gtest/gtest.h"
#include "Transvector/Vector3.hh"
#include <string>

TEST(Vector3_CopyMove, copy_constructor)
{
  Transvector::Vector3<int> vec3_int(34, -83, 36);
  Transvector::Vector3<int> vec3_int3(vec3_int);
  ASSERT_TRUE(vec3_int.x() == vec3_int3.x() && vec3_int.y() == vec3_int3.y() && vec3_int.z() == vec3_int3.z())
    << "Values should be equal";

  Transvector::Vector3<double> vec3_double(34.3, -83.4, 884.11);
  Transvector::Vector3<double> vec3_double3(vec3_double);
  ASSERT_TRUE(vec3_double.x() == vec3_double3.x() && vec3_double.y() == vec3_double3.y() && vec3_double.z() == vec3_double3.z())
    << "Values should be equal";

  Transvector::Vector3<float> vec3_float(34.3f, -83.4f, 36.22f);
  Transvector::Vector3<float> vec3_float3(vec3_float);
  ASSERT_TRUE(vec3_float.x() == vec3_float3.x() && vec3_float.y() == vec3_float3.y() && vec3_float.z() == vec3_float3.z())
    << "Values should be equal";
}

TEST(Vector3_CopyMove, copy_assignment)
{
  Transvector::Vector3<int> vec3_int(34, -83, 36);
  Transvector::Vector3<int> vec3_int3(13);
  vec3_int3 = vec3_int;
  ASSERT_TRUE(vec3_int.x() == vec3_int3.x() && vec3_int.y() == vec3_int3.y() && vec3_int.z() == vec3_int3.z())
    << "Values should be equal";

  Transvector::Vector3<double> vec3_double(34.3, -83.4, 884.11);
  Transvector::Vector3<double> vec3_double3(43.53);
  vec3_double3 = vec3_double;
  ASSERT_TRUE(vec3_double.x() == vec3_double3.x() && vec3_double.y() == vec3_double3.y() && vec3_double.z() == vec3_double3.z())
    << "Values should be equal";

  Transvector::Vector3<float> vec3_float(34.3f, -83.4f, 36.22f);
  Transvector::Vector3<float> vec3_float3(84.3f);
  vec3_float3 = vec3_float;
  ASSERT_TRUE(vec3_float.x() == vec3_float3.x() && vec3_float.y() == vec3_float3.y() && vec3_float.z() == vec3_float3.z())
    << "Values should be equal";
}

TEST(Vector3_CopyMove, move_constructor)
{
  Transvector::Vector3<int> vec3_int(34, -83, 36);
  Transvector::Vector3<int> vec3_int3(std::move(vec3_int));
  ASSERT_TRUE(vec3_int3.x() == 34 && vec3_int3.y() == -83 && vec3_int3.z() == 36);

  Transvector::Vector3<double> vec3_double(34.3, -83.4, 884.11);
  Transvector::Vector3<double> vec3_double3(std::move(vec3_double));
  ASSERT_TRUE(vec3_double3.x() == 34.3 && vec3_double3.y() == -83.4 && vec3_double3.z() == 884.11);

  Transvector::Vector3<float> vec3_float(34.3f, -83.4f, 36.22f);
  Transvector::Vector3<float> vec3_float3(std::move(vec3_float));
  ASSERT_TRUE(vec3_float3.x() == 34.3f && vec3_float3.y() == -83.4f && vec3_float3.z() == 36.22f);
}

TEST(Vector3_CopyMove, move_assignment)
{
  Transvector::Vector3<int> vec3_int(34, -83, 36);
  Transvector::Vector3<int> vec3_int3(13);
  vec3_int3 = std::move(vec3_int);
  ASSERT_TRUE(vec3_int3.x() == 34 && vec3_int3.y() == -83 && vec3_int3.z() == 36);

  Transvector::Vector3<double> vec3_double(34.3, -83.4, 884.11);
  Transvector::Vector3<double> vec3_double3(43.53);
  vec3_double3 = std::move(vec3_double);
  ASSERT_TRUE(vec3_double3.x() == 34.3 && vec3_double3.y() == -83.4 && vec3_double3.z() == 884.11);

  Transvector::Vector3<float> vec3_float(34.3f, -83.4f, 36.22f);
  Transvector::Vector3<float> vec3_float3(84.3f);
  vec3_float3 = std::move(vec3_float);
  ASSERT_TRUE(vec3_float3.x() == 34.3f && vec3_float3.y() == -83.4f && vec3_float3.z() == 36.22f);
}

int main(int ac, char** av)
{
  testing::InitGoogleTest(&ac, av);
  return (RUN_ALL_TESTS());
}
