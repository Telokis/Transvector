#include "gtest/gtest.h"
#include "Transvector/Vector2.hh"
#include "Transvector/Vector.hh"
#include "Transvector/VectorCast.hh"
#include "Transvector/VectorTraits/Bullet.hh"

TEST(VectorTraits_Bullet, BasicOperations)
{
  btVector3 sf_vec3(17, 82, 37);
  Transvector::Vector2<int> vec2_int(14, 17);

  const auto vec3_double = Transvector::GenericVectorCast<double, 3>(sf_vec3);
  ASSERT_TRUE(vec3_double.x() == 17.0
              && vec3_double.y() == 82.0
              && vec3_double.z() == 37.0);

  const auto vec4_float = Transvector::GenericVectorCast<float, 4>(sf_vec3);
  ASSERT_TRUE(vec4_float.at(0) == 17.0f
              && vec4_float.at(1) == 82.0f
              && vec4_float.at(2) == 37.0f
              && vec4_float.at(3) == 0.0f);

  const auto vec2_int_added = vec2_int + sf_vec3;
  ASSERT_TRUE(vec2_int_added.x() == 14 + 17
              && vec2_int_added.y() == 17 + 82);

  const auto vec2_int_casted = Transvector::VectorCast<btVector3>(vec4_float);
  ASSERT_TRUE(vec2_int_casted.x() == 17.0f
              && vec2_int_casted.y() == 82.0f
              && vec2_int_casted.z() == 37.0f);
}

int main(int ac, char** av)
{
  testing::InitGoogleTest(&ac, av);
  return (RUN_ALL_TESTS());
}
