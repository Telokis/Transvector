#include "gtest/gtest.h"
#include "Transvector/Vector2.hh"
#include "Transvector/Vector.hh"
#include "Transvector/VectorCast.hh"
#include "Transvector/VectorTraits/SFML.hh"

TEST(VectorTraits_SFML, BasicOperations)
{
  sf::Vector2<int> sf_vec2_int(17, 82);
  Transvector::Vector2<int> vec2_int(14, 17);

  const auto vec3_double = Transvector::GenericVectorCast<double, 3>(sf_vec2_int);
  ASSERT_TRUE(vec3_double.x() == 17.0
              && vec3_double.y() == 82.0
              && vec3_double.z() == 0.0);

  const auto vec4_float = Transvector::GenericVectorCast<float, 4>(sf_vec2_int);
  ASSERT_TRUE(vec4_float.at(0) == 17.0f
              && vec4_float.at(1) == 82.0f
              && vec4_float.at(2) == 0.0f
              && vec4_float.at(3) == 0.0f);

  const auto vec2_int_added = vec2_int + sf_vec2_int;
  ASSERT_TRUE(vec2_int_added.x() == 14 + 17
              && vec2_int_added.y() == 17 + 82);

  const auto vec3_float_casted = Transvector::VectorCast<sf::Vector3<float>>(vec4_float);
  ASSERT_TRUE(vec3_float_casted.x == 17.0f
              && vec3_float_casted.y == 82.0f
              && vec3_float_casted.z == 0.0f);

  ASSERT_THROW(Transvector::VectorTraits<decltype(sf_vec2_int)>::at(sf_vec2_int, 3),
               std::out_of_range);
  ASSERT_THROW(Transvector::VectorTraits<sf::Vector3<float>>::at(vec3_float_casted, 5),
               std::out_of_range);
}

int main(int ac, char** av)
{
  testing::InitGoogleTest(&ac, av);
  return (RUN_ALL_TESTS());
}
