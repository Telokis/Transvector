#include "gtest/gtest.h"
#include "Transvector/Vector2.hh"
#include "Transvector/Vector.hh"
#include "Transvector/VectorCast.hh"
#include "Transvector/VectorTraits/Ogre.hh"

TEST(VectorTraits_OGRE, BasicOperations)
{
  Ogre::Vector2 og_vec2_i(17, 82);
  Ogre::Vector3 og_vec3_f(17.94f, 82.088f, 71.94f);
  Transvector::Vector2<int> vec2_int(14, 17);

  const auto vec3_double = Transvector::GenericVectorCast<double, 3>(og_vec2_i);
  ASSERT_TRUE(vec3_double.x() == 17.0
              && vec3_double.y() == 82.0
              && vec3_double.z() == 0.0);

  const auto vec4_float = Transvector::GenericVectorCast<float, 4>(og_vec3_f);
  ASSERT_TRUE(vec4_float.at(0) == 17.94f
              && vec4_float.at(1) == 82.088f
              && vec4_float.at(2) == 71.94f
              && vec4_float.at(3) == 0.0f);

  const auto vec2_int_added = vec2_int + og_vec2_i;
  ASSERT_TRUE(vec2_int_added.x() == 14 + 17
              && vec2_int_added.y() == 17 + 82);

  const auto vec2_int_casted = Transvector::VectorCast<Ogre::Vector4>(vec4_float);
  ASSERT_TRUE(vec2_int_casted.x == 17.94f
              && vec2_int_casted.y == 82.088f
              && vec2_int_casted.z == 71.94f
              && vec2_int_casted.w == 0.0f);
}

int main(int ac, char** av)
{
  testing::InitGoogleTest(&ac, av);
  return (RUN_ALL_TESTS());
}
