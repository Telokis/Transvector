#include "gtest/gtest.h"
#include "Transvector/Vector2.hh"
#include <string>

TEST(Vector2_Construction, default)
{
  Transvector::Vector2<int> vec2_int;
  ASSERT_TRUE(vec2_int.x() == 0 && vec2_int.y() == 0)
    << "Values should initialize to 0";

  Transvector::Vector2<double> vec2_double;
  ASSERT_TRUE(vec2_double.x() == 0.0 && vec2_double.y() == 0.0)
    << "Values should initialize to 0.0";

  Transvector::Vector2<float> vec2_float;
  ASSERT_TRUE(vec2_float.x() == 0.0f && vec2_float.y() == 0.0f)
    << "Values should initialize to 0.0f";
}

TEST(Vector2_Construction, values)
{
  Transvector::Vector2<int> vec2_int(34, -82);
  ASSERT_TRUE(vec2_int.x() == 34 && vec2_int.y() == -82)
    << "Values should be {x = 34, y = -82}";

  Transvector::Vector2<double> vec2_double(114.351, -251.847);
  ASSERT_TRUE(vec2_double.x() == 114.351 && vec2_double.y() == -251.847)
    << "Values should be {x = 114.351, y = -251.847}";

  Transvector::Vector2<float> vec2_float(954.333f, -822.015f);
  ASSERT_TRUE(vec2_float.x() == 954.333f && vec2_float.y() == -822.015f)
    << "Values should be {x = 954.333f, y = -822.015f}";
}

TEST(Vector2_Construction, fill)
{
  Transvector::Vector2<int> vec2_int(28);
  ASSERT_TRUE(vec2_int.x() == 28 && vec2_int.y() == 28)
    << "Values should be 28";

  Transvector::Vector2<double> vec2_double(43649.26);
  ASSERT_TRUE(vec2_double.x() == 43649.26 && vec2_double.y() == 43649.26)
    << "Values should be 43649.26";

  Transvector::Vector2<float> vec2_float(5184.33f);
  ASSERT_TRUE(vec2_float.x() == 5184.33f && vec2_float.y() == 5184.33f)
    << "Values should be 5184.33f";
}

template <class T>
bool implicit_initialization(const Transvector::Vector2<T>& vec, T expected_value)
{
  return vec.x() == expected_value && vec.y() == expected_value;
}

TEST(Vector2_Construction, fill_implicit)
{
  bool res_int = implicit_initialization<int>(128, 128);
  ASSERT_TRUE(res_int)
    << "Fill constructor should be implicit";

  bool res_double = implicit_initialization<double>(58.35, 58.35);
  ASSERT_TRUE(res_double)
    << "Fill constructor should be implicit";

  bool res_float = implicit_initialization<float>(33.14f, 33.14f);
  ASSERT_TRUE(res_float)
    << "Fill constructor should be implicit";
}

int main(int ac, char** av)
{
  testing::InitGoogleTest(&ac, av);
  return (RUN_ALL_TESTS());
}
