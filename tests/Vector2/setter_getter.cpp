#include "gtest/gtest.h"
#include "Transvector/Vector2.hh"
#include <string>

TEST(Vector2_SetGet, set_xy)
{
  Transvector::Vector2<int> vec2_int(34, -82);
  ASSERT_TRUE(vec2_int.x() == 34 && vec2_int.y() == -82);
  vec2_int.x(1184).y(-224);
  ASSERT_TRUE(vec2_int.x() == 1184 && vec2_int.y() == -224);

  Transvector::Vector2<double> vec2_double(114.351, -251.847);
  ASSERT_TRUE(vec2_double.x() == 114.351 && vec2_double.y() == -251.847);
  vec2_double.x(1184.1748).y(-224.33);
  ASSERT_TRUE(vec2_double.x() == 1184.1748 && vec2_double.y() == -224.33);

  Transvector::Vector2<float> vec2_float(954.333f, -822.015f);
  ASSERT_TRUE(vec2_float.x() == 954.333f && vec2_float.y() == -822.015f);
  vec2_float.x(345.2f).y(-224.88f);
  ASSERT_TRUE(vec2_float.x() == 345.2f && vec2_float.y() == -224.88f);
}

TEST(Vector2_SetGet, set_set)
{
  Transvector::Vector2<int> vec2_int(34, -82);
  ASSERT_TRUE(vec2_int.x() == 34 && vec2_int.y() == -82);
  vec2_int.set(1184, -224);
  ASSERT_TRUE(vec2_int.x() == 1184 && vec2_int.y() == -224);

  Transvector::Vector2<double> vec2_double(114.351, -251.847);
  ASSERT_TRUE(vec2_double.x() == 114.351 && vec2_double.y() == -251.847);
  vec2_double.set(1184.1748, -224.33);
  ASSERT_TRUE(vec2_double.x() == 1184.1748 && vec2_double.y() == -224.33);

  Transvector::Vector2<float> vec2_float(954.333f, -822.015f);
  ASSERT_TRUE(vec2_float.x() == 954.333f && vec2_float.y() == -822.015f);
  vec2_float.set(345.2f, -224.88f);
  ASSERT_TRUE(vec2_float.x() == 345.2f && vec2_float.y() == -224.88f);
}

TEST(Vector2_SetGet, get_xy)
{
  const Transvector::Vector2<int> vec2_int(34, -82);
  ASSERT_TRUE(vec2_int.x() == 34 && vec2_int.y() == -82);

  const Transvector::Vector2<double> vec2_double(114.351, -251.847);
  ASSERT_TRUE(vec2_double.x() == 114.351 && vec2_double.y() == -251.847);

  const Transvector::Vector2<float> vec2_float(954.333f, -822.015f);
  ASSERT_TRUE(vec2_float.x() == 954.333f && vec2_float.y() == -822.015f);
}

TEST(Vector2_SetGet, set_at)
{
  Transvector::Vector2<int> vec2_int(34, -82);
  vec2_int.at(0) = 1184;
  vec2_int.at(1) = -224;
  ASSERT_TRUE(vec2_int.x() == 1184 && vec2_int.y() == -224);

  Transvector::Vector2<double> vec2_double(114.351, -251.847);
  vec2_double.at(0) = 1184.1748;
  vec2_double.at(1) = -224.33;
  ASSERT_TRUE(vec2_double.x() == 1184.1748 && vec2_double.y() == -224.33);

  Transvector::Vector2<float> vec2_float(954.333f, -822.015f);
  vec2_float.at(0) = 345.2f;
  vec2_float.at(1) = -224.88f;
  ASSERT_TRUE(vec2_float.x() == 345.2f && vec2_float.y() == -224.88f);
}

TEST(Vector2_SetGet, get_at)
{
  const Transvector::Vector2<int> vec2_int(34, -82);
  ASSERT_TRUE(vec2_int.at(0) == 34 && vec2_int.at(1) == -82);

  const Transvector::Vector2<double> vec2_double(114.351, -251.847);
  ASSERT_TRUE(vec2_double.at(0) == 114.351 && vec2_double.at(1) == -251.847);

  const Transvector::Vector2<float> vec2_float(954.333f, -822.015f);
  ASSERT_TRUE(vec2_float.at(0) == 954.333f && vec2_float.at(1) == -822.015f);
}

int main(int ac, char** av)
{
  testing::InitGoogleTest(&ac, av);
  return (RUN_ALL_TESTS());
}
