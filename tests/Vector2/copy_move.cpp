#include "gtest/gtest.h"
#include "Transvector/Vector2.hh"
#include <string>

TEST(Vector2_CopyMove, copy_constructor)
{
  Transvector::Vector2<int> vec2_int(34, -82);
  Transvector::Vector2<int> vec2_int2(vec2_int);
  ASSERT_TRUE(vec2_int.x() == vec2_int2.x() && vec2_int.y() == vec2_int2.y())
    << "Values should be equal";

  Transvector::Vector2<double> vec2_double(34.3, -82.4);
  Transvector::Vector2<double> vec2_double2(vec2_double);
  ASSERT_TRUE(vec2_double.x() == vec2_double2.x() && vec2_double.y() == vec2_double2.y())
    << "Values should be equal";

  Transvector::Vector2<float> vec2_float(34.3f, -82.4f);
  Transvector::Vector2<float> vec2_float2(vec2_float);
  ASSERT_TRUE(vec2_float.x() == vec2_float2.x() && vec2_float.y() == vec2_float2.y())
    << "Values should be equal";
}

TEST(Vector2_CopyMove, copy_assignment)
{
  Transvector::Vector2<int> vec2_int(34, -82);
  Transvector::Vector2<int> vec2_int2(12);
  vec2_int2 = vec2_int;
  ASSERT_TRUE(vec2_int.x() == vec2_int2.x() && vec2_int.y() == vec2_int2.y())
    << "Values should be equal";

  Transvector::Vector2<double> vec2_double(34.3, -82.4);
  Transvector::Vector2<double> vec2_double2(43.52);
  vec2_double2 = vec2_double;
  ASSERT_TRUE(vec2_double.x() == vec2_double2.x() && vec2_double.y() == vec2_double2.y())
    << "Values should be equal";

  Transvector::Vector2<float> vec2_float(34.3f, -82.4f);
  Transvector::Vector2<float> vec2_float2(84.2f);
  vec2_float2 = vec2_float;
  ASSERT_TRUE(vec2_float.x() == vec2_float2.x() && vec2_float.y() == vec2_float2.y())
    << "Values should be equal";
}

TEST(Vector2_CopyMove, move_constructor)
{
  Transvector::Vector2<int> vec2_int(34, -82);
  Transvector::Vector2<int> vec2_int2(std::move(vec2_int));
  ASSERT_TRUE(vec2_int2.x() == 34 && vec2_int2.y() == -82);

  Transvector::Vector2<double> vec2_double(34.3, -82.4);
  Transvector::Vector2<double> vec2_double2(std::move(vec2_double));
  ASSERT_TRUE(vec2_double2.x() == 34.3 && vec2_double2.y() == -82.4);

  Transvector::Vector2<float> vec2_float(34.3f, -82.4f);
  Transvector::Vector2<float> vec2_float2(std::move(vec2_float));
  ASSERT_TRUE(vec2_float2.x() == 34.3f && vec2_float2.y() == -82.4f);
}

TEST(Vector2_CopyMove, move_assignment)
{
  Transvector::Vector2<int> vec2_int(34, -82);
  Transvector::Vector2<int> vec2_int2(12);
  vec2_int2 = std::move(vec2_int);
  ASSERT_TRUE(vec2_int2.x() == 34 && vec2_int2.y() == -82);

  Transvector::Vector2<double> vec2_double(34.3, -82.4);
  Transvector::Vector2<double> vec2_double2(43.52);
  vec2_double2 = std::move(vec2_double);
  ASSERT_TRUE(vec2_double2.x() == 34.3 && vec2_double2.y() == -82.4);

  Transvector::Vector2<float> vec2_float(34.3f, -82.4f);
  Transvector::Vector2<float> vec2_float2(84.2f);
  vec2_float2 = std::move(vec2_float);
  ASSERT_TRUE(vec2_float2.x() == 34.3f && vec2_float2.y() == -82.4f);
}

int main(int ac, char** av)
{
  testing::InitGoogleTest(&ac, av);
  return (RUN_ALL_TESTS());
}
