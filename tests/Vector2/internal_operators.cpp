#include "gtest/gtest.h"
#include "Transvector/Vector2.hh"
#include <string>

TEST(Vector2_InternalOperators, addition_vector)
{
  Transvector::Vector2<int> vec2_int(24, 35);
  vec2_int += Transvector::Vector2<int>(37, -20);
  ASSERT_TRUE(vec2_int.x() == 24 + 37 && vec2_int.y() == 35 + -20)
    << "Values should add properly";

  Transvector::Vector2<double> vec2_double(24.64, 35.18);
  vec2_double += Transvector::Vector2<double>(37.3375, -20.0015);
  ASSERT_TRUE(vec2_double.x() == 24.64 + 37.3375 && vec2_double.y() == 35.18 + -20.0015)
    << "Values should add properly";

  Transvector::Vector2<float> vec2_float(24.64f, 35.18f);
  vec2_float += Transvector::Vector2<float>(37.3375f, -20.0015f);
  ASSERT_TRUE(vec2_float.x() == 24.64f + 37.3375f && vec2_float.y() == 35.18f + -20.0015f)
    << "Values should add properly";
}

TEST(Vector2_InternalOperators, addition_value)
{
  Transvector::Vector2<int> vec2_int(24, 35);
  vec2_int += 17;
  ASSERT_TRUE(vec2_int.x() == 24 + 17 && vec2_int.y() == 35 + 17)
    << "Values should add properly";

  Transvector::Vector2<double> vec2_double(24.64, 35.18);
  vec2_double += -2780.2618;
  ASSERT_TRUE(vec2_double.x() == 24.64 + -2780.2618 && vec2_double.y() == 35.18 + -2780.2618)
    << "Values should add properly";

  Transvector::Vector2<float> vec2_float(24.64f, 35.18f);
  vec2_float += 589.236f;
  ASSERT_TRUE(vec2_float.x() == 24.64f + 589.236f && vec2_float.y() == 35.18f + 589.236f)
    << "Values should add properly";
}

TEST(Vector2_InternalOperators, subtraction_vector)
{
  Transvector::Vector2<int> vec2_int(24, 35);
  vec2_int -= Transvector::Vector2<int>(37, -20);
  ASSERT_TRUE(vec2_int.x() == 24 - 37 && vec2_int.y() == 35 - -20)
    << "Values should subtract properly";

  Transvector::Vector2<double> vec2_double(24.64, 35.18);
  vec2_double -= Transvector::Vector2<double>(37.3375, -20.0015);
  ASSERT_TRUE(vec2_double.x() == 24.64 - 37.3375 && vec2_double.y() == 35.18 - -20.0015)
    << "Values should subtract properly";

  Transvector::Vector2<float> vec2_float(24.64f, 35.18f);
  vec2_float -= Transvector::Vector2<float>(37.3375f, -20.0015f);
  ASSERT_TRUE(vec2_float.x() == 24.64f - 37.3375f && vec2_float.y() == 35.18f - -20.0015f)
    << "Values should subtract properly";
}

TEST(Vector2_InternalOperators, subtraction_value)
{
  Transvector::Vector2<int> vec2_int(24, 35);
  vec2_int -= 17;
  ASSERT_TRUE(vec2_int.x() == 24 - 17 && vec2_int.y() == 35 - 17)
    << "Values should subtract properly";

  Transvector::Vector2<double> vec2_double(24.64, 35.18);
  vec2_double -= -2780.2618;
  ASSERT_TRUE(vec2_double.x() == 24.64 - -2780.2618 && vec2_double.y() == 35.18 - -2780.2618)
    << "Values should subtract properly";

  Transvector::Vector2<float> vec2_float(24.64f, 35.18f);
  vec2_float -= 589.236f;
  ASSERT_TRUE(vec2_float.x() == 24.64f - 589.236f && vec2_float.y() == 35.18f - 589.236f)
    << "Values should subtract properly";
}

TEST(Vector2_InternalOperators, multiplication_vector)
{
  Transvector::Vector2<int> vec2_int(24, 35);
  vec2_int *= Transvector::Vector2<int>(37, -20);
  ASSERT_TRUE(vec2_int.x() == 24 * 37 && vec2_int.y() == 35 * -20)
    << "Values should multiply properly";

  Transvector::Vector2<double> vec2_double(24.64, 35.18);
  vec2_double *= Transvector::Vector2<double>(37.3375, -20.0015);
  ASSERT_TRUE(vec2_double.x() == 24.64 * 37.3375 && vec2_double.y() == 35.18 * -20.0015)
    << "Values should multiply properly";

  Transvector::Vector2<float> vec2_float(24.64f, 35.18f);
  vec2_float *= Transvector::Vector2<float>(37.3375f, -20.0015f);
  ASSERT_TRUE(vec2_float.x() == 24.64f * 37.3375f && vec2_float.y() == 35.18f * -20.0015f)
    << "Values should multiply properly";
}

TEST(Vector2_InternalOperators, multiplication_value)
{
  Transvector::Vector2<int> vec2_int(24, 35);
  vec2_int *= 17;
  ASSERT_TRUE(vec2_int.x() == 24 * 17 && vec2_int.y() == 35 * 17)
    << "Values should multiply properly";

  Transvector::Vector2<double> vec2_double(24.64, 35.18);
  vec2_double *= -2780.2618;
  ASSERT_TRUE(vec2_double.x() == 24.64 * -2780.2618 && vec2_double.y() == 35.18 * -2780.2618)
    << "Values should multiply properly";

  Transvector::Vector2<float> vec2_float(24.64f, 35.18f);
  vec2_float *= 589.236f;
  ASSERT_TRUE(vec2_float.x() == 24.64f * 589.236f && vec2_float.y() == 35.18f * 589.236f)
    << "Values should multiply properly";
}

TEST(Vector2_InternalOperators, division_vector)
{
  Transvector::Vector2<int> vec2_int(24, 35);
  vec2_int /= Transvector::Vector2<int>(37, -20);
  ASSERT_TRUE(vec2_int.x() == 24 / 37 && vec2_int.y() == 35 / -20)
    << "Values should divide properly";

  Transvector::Vector2<double> vec2_double(24.64, 35.18);
  vec2_double /= Transvector::Vector2<double>(37.3375, -20.0015);
  ASSERT_TRUE(vec2_double.x() == 24.64 / 37.3375 && vec2_double.y() == 35.18 / -20.0015)
    << "Values should divide properly";

  Transvector::Vector2<float> vec2_float(24.64f, 35.18f);
  vec2_float /= Transvector::Vector2<float>(37.3375f, -20.0015f);
  ASSERT_TRUE(vec2_float.x() == 24.64f / 37.3375f && vec2_float.y() == 35.18f / -20.0015f)
    << "Values should divide properly";
}

TEST(Vector2_InternalOperators, division_value)
{
  Transvector::Vector2<int> vec2_int(24, 35);
  vec2_int /= 17;
  ASSERT_TRUE(vec2_int.x() == 24 / 17 && vec2_int.y() == 35 / 17)
    << "Values should divide properly";

  Transvector::Vector2<double> vec2_double(24.64, 35.18);
  vec2_double /= -2780.2618;
  ASSERT_TRUE(vec2_double.x() == 24.64 / -2780.2618 && vec2_double.y() == 35.18 / -2780.2618)
    << "Values should divide properly";

  Transvector::Vector2<float> vec2_float(24.64f, 35.18f);
  vec2_float /= 589.236f;
  ASSERT_TRUE(vec2_float.x() == 24.64f / 589.236f && vec2_float.y() == 35.18f / 589.236f)
    << "Values should divide properly";
}

int main(int ac, char** av)
{
  testing::InitGoogleTest(&ac, av);
  return (RUN_ALL_TESTS());
}
