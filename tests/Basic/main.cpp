#include <iostream>
#include <string>
#include <Transvector/VectorTraits.hh>
#include <Transvector/Vector.hh>

namespace Traits = Transvector::Traits;

struct FakeVector
{
  float x, y;
};

namespace Transvector
{
  template <>
  struct VectorTraits < FakeVector >
  {
    typedef FakeVector          VectorType;
    typedef float               ValueType;
    static const std::uint16_t  dimension = 2;

    static ValueType& at(FakeVector& t, std::uint16_t i)
    {
      switch (i)
      {
        case 0:
          return t.x;
        case 1:
          return t.y;
        default:
          throw std::out_of_range("Invalid range for vector FakeVector");
      }
    }

    static const ValueType& at(const FakeVector& t, std::uint16_t i)
    {
      switch (i)
      {
        case 0:
          return t.x;
        case 1:
          return t.y;
        default:
          throw std::out_of_range("Invalid range for vector FakeVector");
      }
    }
  };
}

int main()
{
  Transvector::Vector2<int> v2(12, -17);
  Transvector::Vector3<double> v3(-75.4, -11.9, 154.56);
  Transvector::GenericVector<int, 4> g1(v3);

  FakeVector fv{1.3f, 3.3f};

  //Transvector::Vector2<int> v21(v3);
  //Transvector::Vector3<float> v31(v21);

  std::cout << fv << "\n";
  //std::cout << Transvector::Vector2<int>(fv) << "\n";
  std::cout << v2 << "\n";
  std::cout << v3 << "\n";
  //std::cout << v21 << "\n";
  //std::cout << v31 << "\n";
  std::cout << g1 << "\n";

  //std::cout << "\n" << (g1 + v2 - v3 * v21 / v31) << "\n";
  return 0;
}
