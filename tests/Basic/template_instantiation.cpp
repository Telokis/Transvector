#include "Transvector/Vector.hh"
#include "Transvector/VectorCast.hh"

namespace Transvector
{
  template class GenericVector<int, 2>;
  template class GenericVector<int, 3>;
  template class GenericVector<int, 4>;

  template const GenericVector<int, 4> operator+(const GenericVector<int, 4>&, const GenericVector<int, 4>&);
  template const GenericVector<int, 4> operator-(const GenericVector<int, 4>&, const GenericVector<int, 4>&);
  template const GenericVector<int, 4> operator*(const GenericVector<int, 4>&, const GenericVector<int, 4>&);
  template const GenericVector<int, 4> operator/(const GenericVector<int, 4>&, const GenericVector<int, 4>&);

  template const GenericVector<int, 4> operator+(const int&, const GenericVector<int, 4>&);
  template const GenericVector<int, 4> operator-(const int&, const GenericVector<int, 4>&);
  template const GenericVector<int, 4> operator*(const int&, const GenericVector<int, 4>&);
  template const GenericVector<int, 4> operator/(const int&, const GenericVector<int, 4>&);

  template const GenericVector<int, 4> operator+(const GenericVector<int, 4>&, const int&);
  template const GenericVector<int, 4> operator-(const GenericVector<int, 4>&, const int&);
  template const GenericVector<int, 4> operator*(const GenericVector<int, 4>&, const int&);
  template const GenericVector<int, 4> operator/(const GenericVector<int, 4>&, const int&);

  template GenericVector<float, 4> detail::VectorCastHelper<GenericVector<float, 4>>(const GenericVector<int, 5>&, std::false_type);
  template GenericVector<float, 4> detail::VectorCastHelper<GenericVector<float, 4>>(const GenericVector<int, 3>&, std::true_type);
  template GenericVector<float, 4> VectorCast<GenericVector<float, 4>>(const GenericVector<int, 5>&);
  template GenericVector<float, 4> GenericVectorCast<float, 4>(const GenericVector<int, 5>&);
  template GenericVector<float, 5> GenericVectorCast<float>(const GenericVector<int, 5>&);
  template GenericVector<float, 5> GenericVectorCast(const GenericVector<float, 5>&);

  template struct VectorTraits<GenericVector<int, 4>>;
} // namespace Transvector

template std::ostream& operator<<(std::ostream&, const Transvector::GenericVector<int, 4>&);

int main()
{
  return 0;
}