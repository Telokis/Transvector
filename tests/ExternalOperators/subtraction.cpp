#include "gtest/gtest.h"
#include "Transvector/Vector2.hh"
#include "Transvector/Vector.hh"

TEST(ExternalOperators_Subtraction, scalar_right)
{
  Transvector::Vector2<int> vec2_int(24, 35);
  auto result_int = vec2_int - -28;
  ASSERT_TRUE(result_int.x() == 24 - -28 && result_int.y() == 35 - -28);

  Transvector::Vector3<double> vec3_double(24.64, 35.18, 22.11);
  auto result_double = vec3_double - 37.14;
  ASSERT_TRUE(result_double.x() == 24.64 - 37.14
              && result_double.y() == 35.18 - 37.14
              && result_double.z() == 22.11 - 37.14);

  Transvector::GenericVector<float, 4> vec4_float(24.64f, 35.18f, 3.4f, -8.01f);
  auto result_float = vec4_float - 3.1415f;
  ASSERT_TRUE(result_float.at(0) == 24.64f - 3.1415f
              && result_float.at(1) == 35.18f - 3.1415f
              && result_float.at(2) == 3.4f - 3.1415f
              && result_float.at(3) == -8.01f - 3.1415f);
}

TEST(ExternalOperators_Subtraction, scalar_left)
{
  Transvector::Vector2<int> vec2_int(24, 35);
  auto result_int = -28 - vec2_int;
  ASSERT_TRUE(result_int.x() == -28 - 24 && result_int.y() == -28 - 35);

  Transvector::Vector3<double> vec3_double(24.64, 35.18, 22.11);
  auto result_double = 37.14 - vec3_double;
  ASSERT_TRUE(result_double.x() == 37.14 - 24.64
              && result_double.y() == 37.14 - 35.18
              && result_double.z() == 37.14 - 22.11);

  Transvector::GenericVector<float, 4> vec4_float(24.64f, 35.18f, 3.4f, -8.01f);
  auto result_float = 3.1415f - vec4_float;
  ASSERT_TRUE(result_float.at(0) == 3.1415f - 24.64f
              && result_float.at(1) == 3.1415f - 35.18f
              && result_float.at(2) == 3.1415f - 3.4f
              && result_float.at(3) == 3.1415f - -8.01f);
}

TEST(ExternalOperators_Subtraction, vector_same)
{
  Transvector::Vector2<int> vec2_int(24, 35);
  auto result_int = vec2_int - Transvector::Vector2<int>(37, -20);
  ASSERT_TRUE(result_int.x() == 24 - 37 && result_int.y() == 35 - -20);

  Transvector::Vector3<double> vec3_double(24.64, 35.18, 22.11);
  auto result_double = vec3_double - Transvector::Vector3<double>(37.3375, -20.0015, 7.1);
  ASSERT_TRUE(result_double.x() == 24.64 - 37.3375
              && result_double.y() == 35.18 - -20.0015
              && result_double.z() == 22.11 - 7.1);

  Transvector::GenericVector<float, 4> vec4_float(24.64f, 35.18f, 3.4f, -8.01f);
  auto result_float = vec4_float - Transvector::GenericVector<float, 4>(37.3375f, -20.0015f, 0.01f, 3.2f);
  ASSERT_TRUE(result_float.at(0) == 24.64f - 37.3375f
              && result_float.at(1) == 35.18f - -20.0015f
              && result_float.at(2) == 3.4f - 0.01f
              && result_float.at(3) == -8.01f - 3.2f);
}

TEST(ExternalOperators_Subtraction, vector_generic_bigger)
{
  Transvector::Vector2<int> vec2_int(24, 35);
  auto result_int = vec2_int - Transvector::GenericVector<int, 5>(37, -20, 9, 2, 4);
  ASSERT_TRUE(result_int.x() == 24 - 37 && result_int.y() == 35 - -20);

  Transvector::Vector3<double> vec3_double(24.64, 35.18, 22.11);
  auto result_double = vec3_double - Transvector::GenericVector<double, 5>(37.3375, -20.0015, 7.1, 2.2, 4.1);
  ASSERT_TRUE(result_double.x() == 24.64 - 37.3375
              && result_double.y() == 35.18 - -20.0015
              && result_double.z() == 22.11 - 7.1);

  Transvector::GenericVector<float, 4> vec4_float(24.64f, 35.18f, 3.4f, -8.01f);
  auto result_float = vec4_float - Transvector::GenericVector<float, 5>(37.3375f, -20.0015f, 0.01f, 3.2f, 4.1f);
  ASSERT_TRUE(result_float.at(0) == 24.64f - 37.3375f
              && result_float.at(1) == 35.18f - -20.0015f
              && result_float.at(2) == 3.4f - 0.01f
              && result_float.at(3) == -8.01f - 3.2f);
}

namespace
{
  template <class T>
  struct FakeVec
  {
    FakeVec(T x, T y)
    : _x(x), _y(y)
    {}

    using ValueType = T;
    static const std::uint16_t dimension = 2;

    T& at(std::uint16_t i)
    {
      switch (i)
      {
        case 0:
          return _x;
        case 1:
          return _y;
        default:
          throw std::logic_error("Out of bounds");
      }
    }

    const T& at(std::uint16_t i) const
    {
      switch (i)
      {
        case 0:
          return _x;
        case 1:
          return _y;
        default:
          throw std::logic_error("Out of bounds");
      }
    }

    T _x;
    T _y;
  };
}

TEST(ExternalOperators_Subtraction, vector_custom_compliant)
{
  Transvector::Vector2<int> vec2_int(24, 35);
  auto result_int = vec2_int - FakeVec<int>(37, -20);
  ASSERT_TRUE(result_int.x() == 24 - 37 && result_int.y() == 35 - -20);

  Transvector::Vector3<double> vec3_double(24.64, 35.18, 22.11);
  auto result_double = vec3_double - FakeVec<double>(37.3375, -20.0015);
  ASSERT_TRUE(result_double.x() == 24.64 - 37.3375
              && result_double.y() == 35.18 - -20.0015
              && result_double.z() == 22.11);

  Transvector::GenericVector<float, 4> vec4_float(24.64f, 35.18f, 3.4f, -8.01f);
  auto result_float = vec4_float - FakeVec<float>(37.3375f, -20.0015f);
  ASSERT_TRUE(result_float.at(0) == 24.64f - 37.3375f
              && result_float.at(1) == 35.18f - -20.0015f
              && result_float.at(2) == 3.4f
              && result_float.at(3) == -8.01f);
}

namespace
{
  template <class T>
  struct FakeVec2
  {
    FakeVec2(T x, T y)
      : _x(x), _y(y)
    {
    }

    T _x;
    T _y;
  };
}

namespace Transvector
{
  template <class T>
  struct VectorTraits<FakeVec2<T>>
  {
    using                       VectorType  = FakeVec2<T>;
    using                       ValueType   = T;
    static const std::uint16_t  dimension   = 2;

    static ValueType& at(VectorType& t, std::uint16_t i)
    {
      switch (i)
      {
        case 0:
          return t._x;
        case 1:
          return t._y;
        default:
          throw std::logic_error("Out of bounds");
      }
    }

    static const ValueType at(const VectorType& t, std::uint16_t i)
    {
      switch (i)
      {
        case 0:
          return t._x;
        case 1:
          return t._y;
        default:
          throw std::logic_error("Out of bounds");
      }
    }
  };
}

TEST(ExternalOperators_Subtraction, vector_custom_specialized)
{
  Transvector::Vector2<int> vec2_int(24, 35);
  auto result_int = vec2_int - FakeVec2<int>(37, -20);
  ASSERT_TRUE(result_int.x() == 24 - 37 && result_int.y() == 35 - -20);

  Transvector::Vector3<double> vec3_double(24.64, 35.18, 22.11);
  auto result_double = vec3_double - FakeVec2<double>(37.3375, -20.0015);
  ASSERT_TRUE(result_double.x() == 24.64 - 37.3375
              && result_double.y() == 35.18 - -20.0015
              && result_double.z() == 22.11);

  Transvector::GenericVector<float, 4> vec4_float(24.64f, 35.18f, 3.4f, -8.01f);
  auto result_float = vec4_float - FakeVec2<float>(37.3375f, -20.0015f);
  ASSERT_TRUE(result_float.at(0) == 24.64f - 37.3375f
              && result_float.at(1) == 35.18f - -20.0015f
              && result_float.at(2) == 3.4f
              && result_float.at(3) == -8.01f);
}

int main(int ac, char** av)
{
  testing::InitGoogleTest(&ac, av);
  return (RUN_ALL_TESTS());
}
