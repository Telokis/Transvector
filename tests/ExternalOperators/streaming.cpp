#include "gtest/gtest.h"
#include "Transvector/Vector2.hh"
#include "Transvector/Vector.hh"
#include <sstream>

TEST(ExternalOperators_Streaming, basic_vector)
{
  std::ostringstream  ss_int;
  Transvector::Vector2<int> vec2_int(24, 35);
  ss_int << vec2_int;
  ASSERT_EQ(ss_int.str(), "Vector<2>(24, 35)");

  std::ostringstream  ss_double;
  Transvector::Vector3<double> vec3_double(24.64, 35.18, 22.11);
  ss_double << vec3_double;
  ASSERT_EQ(ss_double.str(), "Vector<3>(24.64, 35.18, 22.11)");

  std::ostringstream  ss_float;
  Transvector::GenericVector<float, 4> vec4_float(24.64f, 35.18f, 3.4f, -8.01f);
  ss_float << vec4_float;
  ASSERT_EQ(ss_float.str(), "Vector<4>(24.64, 35.18, 3.4, -8.01)");
}

namespace
{
  template <class T>
  struct FakeVec
  {
    FakeVec(T x, T y)
    : _x(x), _y(y)
    {}

    using ValueType = T;
    static const std::uint16_t dimension = 2;

    T& at(std::uint16_t i)
    {
      switch (i)
      {
        case 0:
          return _x;
        case 1:
          return _y;
        default:
          throw std::logic_error("Out of bounds");
      }
    }

    const T& at(std::uint16_t i) const
    {
      switch (i)
      {
        case 0:
          return _x;
        case 1:
          return _y;
        default:
          throw std::logic_error("Out of bounds");
      }
    }

    T _x;
    T _y;
  };
}

TEST(ExternalOperators_Streaming, vector_custom_compliant)
{
  std::ostringstream  ss_int;
  FakeVec<int> vec2_int(24, 35);
  ss_int << vec2_int;
  ASSERT_EQ(ss_int.str(), "Vector<2>(24, 35)");

  std::ostringstream  ss_double;
  FakeVec<double> vec3_double(24.64, 35.18);
  ss_double << vec3_double;
  ASSERT_EQ(ss_double.str(), "Vector<2>(24.64, 35.18)");

  std::ostringstream  ss_float;
  FakeVec<float> vec4_float(24.64f, 35.18f);
  ss_float << vec4_float;
  ASSERT_EQ(ss_float.str(), "Vector<2>(24.64, 35.18)");
}

namespace
{
  template <class T>
  struct FakeVec2
  {
    FakeVec2(T x, T y)
      : _x(x), _y(y)
    {
    }

    T _x;
    T _y;
  };
}

namespace Transvector
{
  template <class T>
  struct VectorTraits<FakeVec2<T>>
  {
    using                       VectorType  = FakeVec2<T>;
    using                       ValueType   = T;
    static const std::uint16_t  dimension   = 2;

    static ValueType& at(VectorType& t, std::uint16_t i)
    {
      switch (i)
      {
        case 0:
          return t._x;
        case 1:
          return t._y;
        default:
          throw std::logic_error("Out of bounds");
      }
    }

    static const ValueType at(const VectorType& t, std::uint16_t i)
    {
      switch (i)
      {
        case 0:
          return t._x;
        case 1:
          return t._y;
        default:
          throw std::logic_error("Out of bounds");
      }
    }
  };
}

TEST(ExternalOperators_Streaming, vector_custom_specialized)
{
  std::ostringstream  ss_int;
  FakeVec2<int> vec2_int(24, 35);
  ss_int << vec2_int;
  ASSERT_EQ(ss_int.str(), "Vector<2>(24, 35)");

  std::ostringstream  ss_double;
  FakeVec2<double> vec3_double(24.64, 35.18);
  ss_double << vec3_double;
  ASSERT_EQ(ss_double.str(), "Vector<2>(24.64, 35.18)");

  std::ostringstream  ss_float;
  FakeVec2<float> vec4_float(24.64f, 35.18f);
  ss_float << vec4_float;
  ASSERT_EQ(ss_float.str(), "Vector<2>(24.64, 35.18)");
}

int main(int ac, char** av)
{
  testing::InitGoogleTest(&ac, av);
  return (RUN_ALL_TESTS());
}
