#include "gtest/gtest.h"
#include "Transvector/GenericVector.hh"
#include <string>

TEST(GenericVector_InternalOperators, addition_vector)
{
  Transvector::GenericVector<int, 4> vec_int(24, 35, 12, 3);
  vec_int += Transvector::GenericVector<int, 4>(37, -20, 3, 4);
  ASSERT_TRUE(vec_int.at(0) == 24 + 37 && vec_int.at(1) == 35 + -20
    && vec_int.at(2) == 12 + 3 && vec_int.at(3) == 3 + 4)
    << "Values should add properly";

  Transvector::GenericVector<double, 4> vec_double(24.64, 35.18, 3.14, 8.15);
  vec_double += Transvector::GenericVector<double, 4>(37.3375, -20.5, 2.3, 4.1);
  ASSERT_TRUE(vec_double.at(0) == 24.64 + 37.3375 && vec_double.at(1) == 35.18 + -20.5
    && vec_double.at(2) == 3.14 + 2.3 && vec_double.at(3) == 8.15 + 4.1)
    << "Values should add properly";

  Transvector::GenericVector<float, 4> vec_float(24.64f, 35.18f, 3.14f, 4.3f);
  vec_float += Transvector::GenericVector<float, 4>(37.3375f, -20.0015f, 3.5f, 3.14f);
  ASSERT_TRUE(vec_float.at(0) == 24.64f + 37.3375f && vec_float.at(1) == 35.18f + -20.0015f
    && vec_float.at(2) == 3.14f + 3.5f && vec_float.at(3) == 4.3f + 3.14f)
    << "Values should add properly";
}

TEST(GenericVector_InternalOperators, addition_value)
{
  Transvector::GenericVector<int, 4> vec_int(24, 35, 12, 3);
  vec_int += 17;
  ASSERT_TRUE(vec_int.at(0) == 24 + 17 && vec_int.at(1) == 35 + 17
    && vec_int.at(2) == 12 + 17 && vec_int.at(3) == 3 + 17)
    << "Values should add properly";

  Transvector::GenericVector<double, 4> vec_double(24.64, 35.18, 3.14, 8.15);
  vec_double += -2780.2618;
  ASSERT_TRUE(vec_double.at(0) == 24.64 + -2780.2618 && vec_double.at(1) == 35.18 + -2780.2618
    && vec_double.at(2) == 3.14 + -2780.2618 && vec_double.at(3) == 8.15 + -2780.2618)
    << "Values should add properly";

  Transvector::GenericVector<float, 4> vec_float(24.64f, 35.18f, 3.14f, 4.3f);
  vec_float += 589.236f;
  ASSERT_TRUE(vec_float.at(0) == 24.64f + 589.236f && vec_float.at(1) == 35.18f + 589.236f
    && vec_float.at(2) == 3.14f + 589.236f && vec_float.at(3) == 4.3f + 589.236f)
    << "Values should add properly";
}

TEST(GenericVector_InternalOperators, subtraction_vector)
{
  Transvector::GenericVector<int, 4> vec_int(24, 35, 12, 3);
  vec_int -= Transvector::GenericVector<int, 4>(37, -20, 3, 4);
  ASSERT_TRUE(vec_int.at(0) == 24 - 37 && vec_int.at(1) == 35 - -20
    && vec_int.at(2) == 12 - 3 && vec_int.at(3) == 3 - 4)
    << "Values should subtract properly";

  Transvector::GenericVector<double, 4> vec_double(24.64, 35.18, 3.14, 8.15);
  vec_double -= Transvector::GenericVector<double, 4>(37.3375, -20.0015, 2.1, 3.14);
  ASSERT_TRUE(vec_double.at(0) == 24.64 - 37.3375 && vec_double.at(1) == 35.18 - -20.0015
    && vec_double.at(2) == 3.14 - 2.1 && vec_double.at(3) == 8.15 - 3.14)
    << "Values should subtract properly";

  Transvector::GenericVector<float, 4> vec_float(24.64f, 35.18f, 3.14f, 4.3f);
  vec_float -= Transvector::GenericVector<float, 4>(37.3375f, -20.0015f, 2.1f, 3.14f);
  ASSERT_TRUE(vec_float.at(0) == 24.64f - 37.3375f && vec_float.at(1) == 35.18f - -20.0015f
    && vec_float.at(2) == 3.14f - 2.1f && vec_float.at(3) == 4.3f - 3.14f)
    << "Values should subtract properly";
}

TEST(GenericVector_InternalOperators, subtraction_value)
{
  Transvector::GenericVector<int, 4> vec_int(24, 35, 12, 3);
  vec_int -= 17;
  ASSERT_TRUE(vec_int.at(0) == 24 - 17 && vec_int.at(1) == 35 - 17
    && vec_int.at(2) == 12 - 17 && vec_int.at(3) == 3 - 17)
    << "Values should subtract properly";

  Transvector::GenericVector<double, 4> vec_double(24.64, 35.18, 3.14, 8.15);
  vec_double -= -2780.2618;
  ASSERT_TRUE(vec_double.at(0) == 24.64 - -2780.2618 && vec_double.at(1) == 35.18 - -2780.2618
    && vec_double.at(2) == 3.14 - -2780.2618 && vec_double.at(3) == 8.15 - -2780.2618)
    << "Values should subtract properly";

  Transvector::GenericVector<float, 4> vec_float(24.64f, 35.18f, 3.14f, 4.3f);
  vec_float -= 589.236f;
  ASSERT_TRUE(vec_float.at(0) == 24.64f - 589.236f && vec_float.at(1) == 35.18f - 589.236f
    && vec_float.at(2) == 3.14f - 589.236f && vec_float.at(3) == 4.3f - 589.236f)
    << "Values should subtract properly";
}

TEST(GenericVector_InternalOperators, multiplication_vector)
{
  Transvector::GenericVector<int, 4> vec_int(24, 35, 12, 3);
  vec_int *= Transvector::GenericVector<int, 4>(37, -20, -2, -3);
  ASSERT_TRUE(vec_int.at(0) == 24 * 37 && vec_int.at(1) == 35 * -20
    && vec_int.at(2) == 12 * -2 && vec_int.at(3) == 3 * -3)
    << "Values should multiply properly";

  Transvector::GenericVector<double, 4> vec_double(24.64, 35.18, 3.14, 8.15);
  vec_double *= Transvector::GenericVector<double, 4>(37.3375, -20.0015, 84.3, 3.14159);
  ASSERT_TRUE(vec_double.at(0) == 24.64 * 37.3375 && vec_double.at(1) == 35.18 * -20.0015
    && vec_double.at(2) == 3.14 * 84.3 && vec_double.at(3) == 8.15 * 3.14159)
    << "Values should multiply properly";

  Transvector::GenericVector<float, 4> vec_float(24.64f, 35.18f, 3.14f, 4.3f);
  vec_float *= Transvector::GenericVector<float, 4>(37.3375f, -20.0015f, 5.4f, 3.14f);
  ASSERT_TRUE(vec_float.at(0) == 24.64f * 37.3375f && vec_float.at(1) == 35.18f * -20.0015f
    && vec_float.at(2) == 3.14f * 5.4f && vec_float.at(3) == 4.3f * 3.14f)
    << "Values should multiply properly";
}

TEST(GenericVector_InternalOperators, multiplication_value)
{
  Transvector::GenericVector<int, 4> vec_int(24, 35, 12, 3);
  vec_int *= 17;
  ASSERT_TRUE(vec_int.at(0) == 24 * 17 && vec_int.at(1) == 35 * 17
    && vec_int.at(2) == 12 * 17 && vec_int.at(3) == 3 * 17)
    << "Values should multiply properly";

  Transvector::GenericVector<double, 4> vec_double(24.64, 35.18, 3.14, 8.15);
  vec_double *= -2780.2618;
  ASSERT_TRUE(vec_double.at(0) == 24.64 * -2780.2618 && vec_double.at(1) == 35.18 * -2780.2618
    && vec_double.at(2) == 3.14 * -2780.2618 && vec_double.at(3) == 8.15 * -2780.2618)
    << "Values should multiply properly";

  Transvector::GenericVector<float, 4> vec_float(24.64f, 35.18f, 3.14f, 4.3f);
  vec_float *= 589.236f;
  ASSERT_TRUE(vec_float.at(0) == 24.64f * 589.236f && vec_float.at(1) == 35.18f * 589.236f
    && vec_float.at(2) == 3.14f * 589.236f && vec_float.at(3) == 4.3f * 589.236f)
    << "Values should multiply properly";
}

TEST(GenericVector_InternalOperators, division_vector)
{
  Transvector::GenericVector<int, 4> vec_int(24, 35, 12, 3);
  vec_int /= Transvector::GenericVector<int, 4>(37, -20, 2, 2);
  ASSERT_TRUE(vec_int.at(0) == 24 / 37 && vec_int.at(1) == 35 / -20
    && vec_int.at(2) == 12 / 2 && vec_int.at(3) == 3 / 2)
    << "Values should divide properly";

  Transvector::GenericVector<double, 4> vec_double(24.64, 35.18, 3.14, 8.15);
  vec_double /= Transvector::GenericVector<double, 4>(37.3375, -20.0015, 5.4, 3.14);
  ASSERT_TRUE(vec_double.at(0) == 24.64 / 37.3375 && vec_double.at(1) == 35.18 / -20.0015
    && vec_double.at(2) == 3.14 / 5.4 && vec_double.at(3) == 8.15 / 3.14)
    << "Values should divide properly";

  Transvector::GenericVector<float, 4> vec_float(24.64f, 35.18f, 3.14f, 4.3f);
  vec_float /= Transvector::GenericVector<float, 4>(37.3375f, -20.0015f, 2.1f, 3.14f);
  ASSERT_TRUE(vec_float.at(0) == 24.64f / 37.3375f && vec_float.at(1) == 35.18f / -20.0015f
    && vec_float.at(2) == 3.14f / 2.1f && vec_float.at(3) == 4.3f / 3.14f)
    << "Values should divide properly";
}

TEST(GenericVector_InternalOperators, division_value)
{
  Transvector::GenericVector<int, 4> vec_int(24, 35, 12, 3);
  vec_int /= 17;
  ASSERT_TRUE(vec_int.at(0) == 24 / 17 && vec_int.at(1) == 35 / 17
    && vec_int.at(2) == 12 / 17 && vec_int.at(3) == 3 / 17)
    << "Values should divide properly";

  Transvector::GenericVector<double, 4> vec_double(24.64, 35.18, 3.14, 8.15);
  vec_double /= -2780.2618;
  ASSERT_TRUE(vec_double.at(0) == 24.64 / -2780.2618 && vec_double.at(1) == 35.18 / -2780.2618
    && vec_double.at(2) == 3.14 / -2780.2618 && vec_double.at(3) == 8.15 / -2780.2618)
    << "Values should divide properly";

  Transvector::GenericVector<float, 4> vec_float(24.64f, 35.18f, 3.14f, 4.3f);
  vec_float /= 589.236f;
  ASSERT_TRUE(vec_float.at(0) == 24.64f / 589.236f && vec_float.at(1) == 35.18f / 589.236f
    && vec_float.at(2) == 3.14f / 589.236f && vec_float.at(3) == 4.3f / 589.236f)
    << "Values should divide properly";
}

int main(int ac, char** av)
{
  testing::InitGoogleTest(&ac, av);
  return (RUN_ALL_TESTS());
}
