#include "gtest/gtest.h"
#include "Transvector/GenericVector.hh"
#include <string>

TEST(GenericVector_CopyMove, copy_constructor)
{
  Transvector::GenericVector<int, 4> vec_int(34, -82, 11, 2);
  Transvector::GenericVector<int, 4> vec_int2(vec_int);
  ASSERT_TRUE(vec_int.at(0) == vec_int2.at(0) && vec_int.at(1) == vec_int2.at(1)
              && vec_int.at(2) == vec_int2.at(2) && vec_int.at(3) == vec_int2.at(3))
    << "Values should be equal";

  Transvector::GenericVector<double, 4> vec_double(34.3, -82.4, 5.3, -2.2);
  Transvector::GenericVector<double, 4> vec_double2(vec_double);
  ASSERT_TRUE(vec_double.at(0) == vec_double2.at(0) && vec_double.at(1) == vec_double2.at(1)
              && vec_double.at(2) == vec_double2.at(2) && vec_double.at(3) == vec_double2.at(3))
    << "Values should be equal";

  Transvector::GenericVector<float, 4> vec_float(34.3f, -82.4f, 74.3f, 5.2f);
  Transvector::GenericVector<float, 4> vec_float2(vec_float);
  ASSERT_TRUE(vec_float.at(0) == vec_float2.at(0) && vec_float.at(1) == vec_float2.at(1)
              && vec_float.at(2) == vec_float2.at(2) && vec_float.at(3) == vec_float2.at(3))
    << "Values should be equal";
}

TEST(GenericVector_CopyMove, copy_assignment)
{
  Transvector::GenericVector<int, 4> vec_int(34, -82, 11, 2);
  Transvector::GenericVector<int, 4> vec_int2(12);
  vec_int2 = vec_int;
  ASSERT_TRUE(vec_int.at(0) == vec_int2.at(0) && vec_int.at(1) == vec_int2.at(1)
              && vec_int.at(2) == vec_int2.at(2) && vec_int.at(3) == vec_int2.at(3))
    << "Values should be equal";

  Transvector::GenericVector<double, 4> vec_double(34.3, -82.4, 5.3, -2.2);
  Transvector::GenericVector<double, 4> vec_double2(43.52);
  vec_double2 = vec_double;
  ASSERT_TRUE(vec_double.at(0) == vec_double2.at(0) && vec_double.at(1) == vec_double2.at(1)
              && vec_double.at(2) == vec_double2.at(2) && vec_double.at(3) == vec_double2.at(3))
    << "Values should be equal";

  Transvector::GenericVector<float, 4> vec_float(34.3f, -82.4f, 74.3f, 5.2f);
  Transvector::GenericVector<float, 4> vec_float2(84.2f);
  vec_float2 = vec_float;
  ASSERT_TRUE(vec_float.at(0) == vec_float2.at(0) && vec_float.at(1) == vec_float2.at(1)
              && vec_float.at(2) == vec_float2.at(2) && vec_float.at(3) == vec_float2.at(3))
    << "Values should be equal";
}

TEST(GenericVector_CopyMove, move_constructor)
{
  Transvector::GenericVector<int, 4> vec_int(34, -82, 8, 77);
  Transvector::GenericVector<int, 4> vec_int2(std::move(vec_int));
  ASSERT_TRUE(vec_int2.at(0) == 34 && vec_int2.at(1) == -82
              && vec_int2.at(2) == 8 && vec_int2.at(3) == 77);

  Transvector::GenericVector<double, 4> vec_double(34.3, -82.4, 3.1, 7.2);
  Transvector::GenericVector<double, 4> vec_double2(std::move(vec_double));
  ASSERT_TRUE(vec_double2.at(0) == 34.3 && vec_double2.at(1) == -82.4
              && vec_double2.at(2) == 3.1 && vec_double2.at(3) == 7.2);

  Transvector::GenericVector<float, 4> vec_float(34.3f, -82.4f, 11.2f, 3.2f);
  Transvector::GenericVector<float, 4> vec_float2(std::move(vec_float));
  ASSERT_TRUE(vec_float2.at(0) == 34.3f && vec_float2.at(1) == -82.4f
              && vec_float2.at(2) == 11.2f && vec_float2.at(3) == 3.2f);
}

TEST(GenericVector_CopyMove, move_assignment)
{
  Transvector::GenericVector<int, 4> vec_int(34, -82, 8, 77);
  Transvector::GenericVector<int, 4> vec_int2(12);
  vec_int2 = std::move(vec_int);
  ASSERT_TRUE(vec_int2.at(0) == 34 && vec_int2.at(1) == -82
              && vec_int2.at(2) == 8 && vec_int2.at(3) == 77);

  Transvector::GenericVector<double, 4> vec_double(34.3, -82.4, 3.1, 7.2);
  Transvector::GenericVector<double, 4> vec_double2(43.52);
  vec_double2 = std::move(vec_double);
  ASSERT_TRUE(vec_double2.at(0) == 34.3 && vec_double2.at(1) == -82.4
              && vec_double2.at(2) == 3.1 && vec_double2.at(3) == 7.2);

  Transvector::GenericVector<float, 4> vec_float(34.3f, -82.4f, 11.2f, 3.2f);
  Transvector::GenericVector<float, 4> vec_float2(84.2f);
  vec_float2 = std::move(vec_float);
  ASSERT_TRUE(vec_float2.at(0) == 34.3f && vec_float2.at(1) == -82.4f
              && vec_float2.at(2) == 11.2f && vec_float2.at(3) == 3.2f);
}

int main(int ac, char** av)
{
  testing::InitGoogleTest(&ac, av);
  return (RUN_ALL_TESTS());
}
