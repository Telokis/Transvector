#include "gtest/gtest.h"
#include "Transvector/GenericVector.hh"
#include <string>

TEST(GenericVector_SetGet, set_set)
{
  Transvector::GenericVector<int, 4> vec_int(34, -82, 44, 77);
  ASSERT_TRUE(vec_int.at(0) == 34 && vec_int.at(1) == -82
              && vec_int.at(2) == 44 && vec_int.at(3) == 77);
  vec_int.set(1184, -224, 88, 20);
  ASSERT_TRUE(vec_int.at(0) == 1184 && vec_int.at(1) == -224
              && vec_int.at(2) == 88 && vec_int.at(3) == 20);

  Transvector::GenericVector<double, 4> vec_double(4.1, -1.7, 0.2, 7.3);
  ASSERT_TRUE(vec_double.at(0) == 4.1 && vec_double.at(1) == -1.7
              && vec_double.at(2) == 0.2 && vec_double.at(3) == 7.3);
  vec_double.set(1184.1748, -224.33, 8.3, 7.01);
  ASSERT_TRUE(vec_double.at(0) == 1184.1748 && vec_double.at(1) == -224.33
              && vec_double.at(2) == 8.3 && vec_double.at(3) == 7.01);

  Transvector::GenericVector<float, 4> vec_float(9.3f, -8.5f, 3.14f, 3.25f);
  ASSERT_TRUE(vec_float.at(0) == 9.3f && vec_float.at(1) == -8.5f
              && vec_float.at(2) == 3.14f && vec_float.at(3) == 3.25f);
  vec_float.set(345.2f, -224.88f, 1.2f, 7.3f);
  ASSERT_TRUE(vec_float.at(0) == 345.2f && vec_float.at(1) == -224.88f
              && vec_float.at(2) == 1.2f && vec_float.at(3) == 7.3f);
}

TEST(GenericVector_SetGet, set_at)
{
  Transvector::GenericVector<int, 4> vec_int(34, -82, 44, 77);
  vec_int.at(0) = 1184;
  vec_int.at(1) = -224;
  vec_int.at(2) = 4;
  vec_int.at(3) = -5;
  ASSERT_TRUE(vec_int.at(0) == 1184 && vec_int.at(1) == -224
              && vec_int.at(2) == 4 && vec_int.at(3) == -5);

  Transvector::GenericVector<double, 4> vec_double(4.1, -1.7, 0.2, 7.3);
  vec_double.at(0) = 1184.1748;
  vec_double.at(1) = -224.33;
  vec_double.at(2) = 4.3;
  vec_double.at(3) = -5.7;
  ASSERT_TRUE(vec_double.at(0) == 1184.1748 && vec_double.at(1) == -224.33
              && vec_double.at(2) == 4.3 && vec_double.at(3) == -5.7);

  Transvector::GenericVector<float, 4> vec_float(9.3f, -8.5f, 3.14f, 3.25f);
  vec_float.at(0) = 345.2f;
  vec_float.at(1) = -224.88f;
  vec_float.at(2) = 4.3f;
  vec_float.at(3) = -5.7f;
  ASSERT_TRUE(vec_float.at(0) == 345.2f && vec_float.at(1) == -224.88f
              && vec_float.at(2) == 4.3f && vec_float.at(3) == -5.7f);
}

TEST(GenericVector_SetGet, get_at)
{
  Transvector::GenericVector<int, 4> vec_int(34, -82, 44, 77);
  ASSERT_TRUE(vec_int.at(0) == 34 && vec_int.at(1) == -82
              && vec_int.at(2) == 44 && vec_int.at(3) == 77);

  Transvector::GenericVector<double, 4> vec_double(4.1, -1.7, 0.2, 7.3);
  ASSERT_TRUE(vec_double.at(0) == 4.1 && vec_double.at(1) == -1.7
              && vec_double.at(2) == 0.2 && vec_double.at(3) == 7.3);

  Transvector::GenericVector<float, 4> vec_float(9.3f, -8.5f, 3.14f, 3.25f);
  ASSERT_TRUE(vec_float.at(0) == 9.3f && vec_float.at(1) == -8.5f
              && vec_float.at(2) == 3.14f && vec_float.at(3) == 3.25f);
}

int main(int ac, char** av)
{
  testing::InitGoogleTest(&ac, av);
  return (RUN_ALL_TESTS());
}
