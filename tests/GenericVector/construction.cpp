#include "gtest/gtest.h"
#include "Transvector/GenericVector.hh"
#include <string>

TEST(GenericVector_Construction, default)
{
  Transvector::GenericVector<int, 4> vec_int;
  ASSERT_TRUE(vec_int.at(0) == 0 && vec_int.at(1) == 0
              && vec_int.at(2) == 0 && vec_int.at(3) == 0)
    << "Values should initialize to 0";

  Transvector::GenericVector<double, 4> vec_double;
  ASSERT_TRUE(vec_double.at(0) == 0.0 && vec_double.at(1) == 0.0
              && vec_double.at(2) == 0.0 && vec_double.at(3) == 0.0)
    << "Values should initialize to 0.0";

  Transvector::GenericVector<float, 4> vec_float;
  ASSERT_TRUE(vec_float.at(0) == 0.0f && vec_float.at(1) == 0.0f
              && vec_float.at(2) == 0.0f && vec_float.at(3) == 0.0f)
    << "Values should initialize to 0.0f";
}

TEST(GenericVector_Construction, values)
{
  Transvector::GenericVector<int, 4> vec_int(34, -82, 12, 92);
  ASSERT_TRUE(vec_int.at(0) == 34 && vec_int.at(1) == -82
              && vec_int.at(2) == 12 && vec_int.at(3) == 92)
    << "Values should be {34, -82, 12, 92}";

  Transvector::GenericVector<double, 4> vec_double(1.1, -1.7, 3.5, -5.2);
  ASSERT_TRUE(vec_double.at(0) == 1.1 && vec_double.at(1) == -1.7
              && vec_double.at(2) == 3.5 && vec_double.at(3) == -5.2)
    << "Values should be {1.1, -1.7, 3.5, -5.2}";

  Transvector::GenericVector<float, 4> vec_float(4.3f, -2.5f, 3.5f, 2.2f);
  ASSERT_TRUE(vec_float.at(0) == 4.3f && vec_float.at(1) == -2.5f
              && vec_float.at(2) == 3.5f && vec_float.at(3) == 2.2f)
    << "Values should be {4.3f, -2.5f, 3.5f, 2.2f}";
}

TEST(GenericVector_Construction, fill)
{
  Transvector::GenericVector<int, 4> vec_int(28);
  ASSERT_TRUE(vec_int.at(0) == 28 && vec_int.at(1) == 28
              && vec_int.at(2) == 28 && vec_int.at(3) == 28)
    << "Values should be 28";

  Transvector::GenericVector<double, 4> vec_double(43649.26);
  ASSERT_TRUE(vec_double.at(0) == 43649.26 && vec_double.at(1) == 43649.26
              && vec_double.at(2) == 43649.26 && vec_double.at(3) == 43649.26)
    << "Values should be 43649.26";

  Transvector::GenericVector<float, 4> vec_float(5184.33f);
  ASSERT_TRUE(vec_float.at(0) == 5184.33f && vec_float.at(1) == 5184.33f
              && vec_float.at(2) == 5184.33f && vec_float.at(3) == 5184.33f)
    << "Values should be 5184.33f";
}

template <class T>
bool implicit_initialization(const Transvector::GenericVector<T, 4>& vec, T expected_value)
{
  return vec.at(0) == expected_value
    && vec.at(1) == expected_value
    && vec.at(2) == expected_value
    && vec.at(3) == expected_value;
}

TEST(GenericVector_Construction, fill_implicit)
{
  bool res_int = implicit_initialization<int>(128, 128);
  ASSERT_TRUE(res_int)
    << "Fill constructor should be implicit";

  bool res_double = implicit_initialization<double>(58.35, 58.35);
  ASSERT_TRUE(res_double)
    << "Fill constructor should be implicit";

  bool res_float = implicit_initialization<float>(33.14f, 33.14f);
  ASSERT_TRUE(res_float)
    << "Fill constructor should be implicit";
}

int main(int ac, char** av)
{
  testing::InitGoogleTest(&ac, av);
  return (RUN_ALL_TESTS());
}
