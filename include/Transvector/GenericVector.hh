#pragma once

#ifndef TRANSVECTOR_GENERICVECTOR_HH_
#define TRANSVECTOR_GENERICVECTOR_HH_

#include <iostream>
#include <cstdint>
#include <array>
#include "Traits/BestType.hh"
#include "VectorTraits.hh"
#include "VectorCast.hh"

namespace	Transvector
{
  template <typename T, std::uint16_t N>
  class		                                      GenericVector
  {
    static_assert(N > 3,
                  "GenericVector can't have dimension below 4");

  public:
    typedef std::decay_t<T>                             ValueType;
    static const std::uint16_t                          dimension = N;
    typedef std::array<ValueType, dimension>            ContainerType;
    typedef Transvector::Traits::BestType_t<ValueType>  BestValueType;

    typedef GenericVector                               MyType;

  public:
    ContainerType   _members;

  public:
    template <
      class... Args,
      typename = std::enable_if_t<sizeof...(Args) == dimension - 1>
    >
      GenericVector(BestValueType first, Args&&... args)
      : _members{{first, std::forward<Args>(args)...}}
    {
    }

    GenericVector(BestValueType a = ValueType())
    {
      _members.fill(a);
    }

    ~GenericVector()
    {
    }

    GenericVector(const GenericVector&) = default;
    GenericVector& operator=(const GenericVector&) = default;
    GenericVector(GenericVector&&) = default;
    GenericVector& operator=(GenericVector&&) = default;

  public:
    // Add
    MyType&      operator+=(const MyType& ref)
    {
      for (std::uint16_t i = 0; i < dimension; ++i)
        _members[i] += ref._members[i];
      return *this;
    }

    // Sub
    MyType&      operator-=(const MyType& ref)
    {
      for (std::uint16_t i = 0; i < dimension; ++i)
        _members[i] -= ref._members[i];
      return *this;
    }

    // Mult
    MyType&      operator*=(const MyType& ref)
    {
      for (std::uint16_t i = 0; i < dimension; ++i)
        _members[i] *= ref._members[i];
      return *this;
    }

    // Div
    MyType&      operator/=(const MyType& ref)
    {
      for (std::uint16_t i = 0; i < dimension; ++i)
        _members[i] /= ref._members[i];
      return *this;
    }

  public:
    template <class... Args>
    MyType& set(BestValueType first, Args&&... args)
    {
      static_assert(sizeof...(Args) == dimension - 1,
                    "Bad number of arguments for set.");
      _members = {{first, std::forward<Args>(args)...}};
      return *this;
    }

  public:
    ValueType& at(std::uint16_t i)
    {
      return _members.at(i);
    }

    constexpr const ValueType& at(std::uint16_t i) const
    {
      return _members.at(i);
    }
  }; // GenericVector

} // namespace Transvector

#endif // TRANSVECTOR_MATH_GENERICVECTOR_HH_