#pragma once

#ifndef TRANSVECTOR_VECTOR_HH_
#define TRANSVECTOR_VECTOR_HH_

#include "Vector2.hh"
#include "Vector3.hh"
#include "VectorTraits.hh"

#ifdef TRANSVECTOR_SHOW_TYPE
#include <typeinfo>
#endif

namespace Transvector
{
  typedef Vector2<float>    Vec2;
  typedef Vector2<float>    Vec2f;
  typedef Vector2<int>      Vec2i;
  typedef Vector2<double>   Vec2d;

  typedef Vector3<float>    Vec3;
  typedef Vector3<float>    Vec3f;
  typedef Vector3<int>      Vec3i;
  typedef Vector3<double>   Vec3d;

  // Generation of arithmetics operators for vectors
#define TRANSVECTOR_VECTOR_GENERATE_MATH_OPERATOR(op) \
    template < \
      class ValueType, \
      std::uint16_t dimension, \
      class U, \
      typename VectorType = GenericVector<ValueType, dimension>, \
      typename Traits = VectorTraits<VectorType>, \
      bool = (VectorTraits<U>::dimension > 0), \
      typename RightTraits = VectorTraits<U> \
    > \
    const VectorType operator op(const GenericVector<ValueType, dimension>& lhs, const U& rhs) \
    { \
      VectorType copy(lhs); \
      std::uint16_t i = 0; \
      \
      for (; i < dimension && i < RightTraits::dimension; ++i) \
        copy.at(i) op ## = static_cast<ValueType>(RightTraits::at(rhs, i)); \
      return copy; \
    } \
    \
    template < \
      class ValueType, \
      class U, \
      std::uint16_t dimension, \
      typename VectorType = GenericVector<ValueType, dimension>, \
      typename Traits = VectorTraits<VectorType>, \
      typename = std::enable_if_t<(std::is_convertible<U, VectorType>::value)>, \
      typename = std::enable_if_t<(!std::is_same<U, VectorType>::value)> \
    > \
    const VectorType operator op(const GenericVector<ValueType, dimension>& lhs, const U& rhs) \
    { \
      VectorType converted(rhs); \
       \
      return lhs op converted; \
    } \
    \
    template < \
      class ValueType, \
      class U, \
      std::uint16_t dimension, \
      typename VectorType = GenericVector<ValueType, dimension>, \
      typename Traits = VectorTraits<VectorType>, \
      typename = std::enable_if_t<(std::is_convertible<U, VectorType>::value)>, \
      typename = std::enable_if_t<(!std::is_same<U, VectorType>::value)> \
    > \
    const VectorType operator op(const U& lhs, const GenericVector<ValueType, dimension>& rhs) \
    { \
      VectorType converted(lhs); \
       \
      return converted op rhs; \
    }

    TRANSVECTOR_VECTOR_GENERATE_MATH_OPERATOR(+)
    TRANSVECTOR_VECTOR_GENERATE_MATH_OPERATOR(-)
    TRANSVECTOR_VECTOR_GENERATE_MATH_OPERATOR(*)
    TRANSVECTOR_VECTOR_GENERATE_MATH_OPERATOR(/)

#undef TRANSVECTOR_VECTOR_GENERATE_MATH_OPERATOR

} // namespace Transvector

// Streaming operator for Vectors
template <
  class T,
  typename = std::enable_if_t<(sizeof(Transvector::VectorTraits<T>) > 0)>,
  typename Traits = Transvector::VectorTraits<T>
>
std::ostream&   operator<<(std::ostream& stream, const T& vec)
{
  stream << "Vector<"
#ifdef TRANSVECTOR_SHOW_TYPE
    << typeid(typename Traits::ValueType).name() << ", "
#endif
    << Traits::dimension << ">(";
  for (std::uint16_t i = 0; i < Traits::dimension; ++i)
  {
    if (i != 0)
      stream << ", ";
    stream << Traits::at(vec, i);
  }
  stream << ")";
  return stream;
}

#endif // TRANSVECTOR_MATH_VECTOR_HH_