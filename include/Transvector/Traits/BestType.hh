#pragma once

#ifndef TRANSVECTOR_TRAITS_BESTTYPE_HH_
#define TRANSVECTOR_TRAITS_BESTTYPE_HH_

#include <type_traits>

namespace Transvector
{
  namespace Traits
  {

    /**
     *  @brief  Traits determining the best type for a function parameter.
     *          If @a T is trivial, the value of @a type will be @a T.
     *          Otherwise, it will be const T&.
     */
    template <typename T>
    using BestType = std::conditional<
      std::is_trivial<T>::value,
      T,
      typename std::add_lvalue_reference<
        typename std::add_const<
          typename std::decay<T>::type
        >::type
      >::type
    >;

    /**
     *  @brief  Shortcut to make usage less verbose.
     */
    template <typename T>
    using BestType_t = typename BestType<T>::type;

  } // namespace Traits
} // namespace Transvector

#endif // TRANSVECTOR_TRAITS_BESTTYPE_HH_