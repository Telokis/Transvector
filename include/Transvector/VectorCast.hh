#pragma once

#ifndef TRANSVECTOR_VECTORCAST_HH_
#define TRANSVECTOR_VECTORCAST_HH_

namespace	Transvector
{
  template <typename T, std::uint16_t N>
  class GenericVector;
}

namespace	Transvector
{
  namespace detail
  {
    /**
     *  @brief  Helper function converting @a inputVec to @a outputVec member
     *          by member. This function is only called if
     *          inputDimension >= outputDimension.
     *
     *  @warning  This function may not be called directly.
     */
    template <
      class OutputVector,
      class InputVector,
      typename OutputTraits = VectorTraits<OutputVector>,
      typename InputTraits = VectorTraits<InputVector>,
      typename OutputValueType = typename OutputTraits::ValueType
    >
    OutputVector VectorCastHelper(const InputVector& inputVec, std::false_type)
    {
      OutputVector outputVec{};
      std::uint16_t i = 0;

      for (; i < OutputTraits::dimension; ++i)
        OutputTraits::at(outputVec, i) = static_cast<OutputValueType>(InputTraits::at(inputVec, i));

      return outputVec;
    }
    
    /**
     *  @brief  Helper function converting @a inputVec to @a outputVec member
     *          by member. This function is only called if
     *          inputDimension < outputDimension.
     *
     *  @warning  This function may not be called directly.
     */
    template <
      class OutputVector,
      class InputVector,
      typename OutputTraits = VectorTraits<OutputVector>,
      class OutputType = typename OutputTraits::ValueType,
      typename InputTraits = VectorTraits<InputVector>
    >
    OutputVector VectorCastHelper(const InputVector& inputVec, std::true_type)
    {
      OutputVector outputVec{};
      std::uint16_t i = 0;

      for (; i < InputTraits::dimension; ++i)
        OutputTraits::at(outputVec, i) = static_cast<OutputType>(InputTraits::at(inputVec, i));

      for (; i < OutputTraits::dimension; ++i)
        OutputTraits::at(outputVec, i) = OutputType();

      return outputVec;
    }
  } // namespace detail

  /**
   *  @brief  
   */
  template <
    class OutputVector,
    class InputVector,
    typename OutputTraits = VectorTraits<OutputVector>,
    typename InputTraits = VectorTraits<InputVector>,
    std::uint16_t outputDimension = OutputTraits::dimension,
    std::uint16_t inputDimension = InputTraits::dimension
  >
  OutputVector VectorCast(const InputVector& inputVec)
  {
    return detail::VectorCastHelper<
      OutputVector
    >(inputVec, std::integral_constant<bool, (inputDimension < outputDimension)>());
  }

  template <
    class OutputType,
    std::uint16_t outputDimension,
    class InputVector,
    class OutputVector = GenericVector<OutputType, outputDimension>
  >
  OutputVector GenericVectorCast(const InputVector& inputVec)
  {
    return VectorCast<OutputVector>(inputVec);
  }

  template <
    class OutputType,
    class InputVector,
    typename InputTraits = VectorTraits<InputVector>,
    class OutputVectorType = GenericVector<OutputType, InputTraits::dimension>
  >
  OutputVectorType  GenericVectorCast(const InputVector& inputVec)
  {
    return GenericVectorCast<OutputType, InputTraits::dimension>(inputVec);
  }

  template <
    class InputVector,
    typename InputTraits = VectorTraits<InputVector>,
    class OutputVectorType = GenericVector<
      typename InputTraits::ValueType,
      InputTraits::dimension
    >
  >
  OutputVectorType  GenericVectorCast(const InputVector& inputVec)
  {
    return GenericVectorCast<
      typename InputTraits::ValueType,
      InputTraits::dimension
    >(inputVec);
  }

} // namespace Transvector

#endif // TRANSVECTOR_MATH_VECTORCAST_HH_
