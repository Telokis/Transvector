#pragma once

#ifndef TRANSVECTOR_VECTOR2_HH_
#define TRANSVECTOR_VECTOR2_HH_

#include <iostream>
#include <array>
#include <cstdint>
#include "GenericVector.hh"

namespace	Transvector
{
  template <typename T>
  class		    GenericVector < T, 2 >
  {
  public:
    typedef std::decay_t<T>                             ValueType;
    static const std::uint16_t                          dimension = 2;
    typedef std::array<ValueType, dimension>            ContainerType;
    typedef Transvector::Traits::BestType_t<ValueType>  BestValueType;

    typedef GenericVector<T, dimension>                 MyType;

  public:
    ContainerType   _members;

  public:
    GenericVector(BestValueType x,
                  BestValueType y)
      : _members{{x, y}}
    {
    }

    GenericVector(BestValueType a = ValueType())
    {
      _members.fill(a);
    }

    ~GenericVector()
    {
    }

    GenericVector(const GenericVector&) = default;
    GenericVector& operator=(const GenericVector&) = default;
    GenericVector(GenericVector&&) = default;
    GenericVector& operator=(GenericVector&&) = default;

  public:
    // Add
    MyType&      operator+=(const MyType& ref)
    {
      _members[0] += ref._members[0];
      _members[1] += ref._members[1];
      return *this;
    }

    // Sub
    MyType&      operator-=(const MyType& ref)
    {
      _members[0] -= ref._members[0];
      _members[1] -= ref._members[1];
      return *this;
    }

    // Mult
    MyType&      operator*=(const MyType& ref)
    {
      _members[0] *= ref._members[0];
      _members[1] *= ref._members[1];
      return *this;
    }

    // Div
    MyType&      operator/=(const MyType& ref)
    {
      _members[0] /= ref._members[0];
      _members[1] /= ref._members[1];
      return *this;
    }

  public:
    // Getters
    decltype(auto)   x() const
    {
      return _members.at(0);
    }

    decltype(auto)   y() const
    {
      return _members.at(1);
    }

    // Setters
    MyType& x(BestValueType val)
    {
      _members[0] = val;
      return *this;
    }

    MyType& y(BestValueType val)
    {
      _members[1] = val;
      return *this;
    }

    MyType& set(BestValueType valX, BestValueType valY)
    {
      _members[0] = valX;
      _members[1] = valY;
      return *this;
    }

  public:
    ValueType& at(std::uint16_t i)
    {
      return _members.at(i);
    }

    constexpr const ValueType& at(std::uint16_t i) const
    {
      return _members.at(i);
    }
  }; // Vector2

  template <typename T>
  using Vector2 = GenericVector < T, 2 >;

} // namespace Transvector

#endif // TRANSVECTOR_MATH_VECTOR2_HH_