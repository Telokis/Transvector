#pragma once

#ifndef TRANSVECTOR_VECTORTRAITS_HH_
#define TRANSVECTOR_VECTORTRAITS_HH_

#include <cstdint>

namespace Transvector
{
  /**
   *  @brief  @ref VectorTraits is the core of Transvector.
   *          It is used to recognize and manipulate a Vector of any kind.
   *          This version is an incomplete struct so that there is no
   *          @ref VectorTraits definition unless partially specialized or
   *          some conditions are met.
   *
   *  @tparam Vec The Vector.
   */
  template <typename Vec, typename = void>
  struct VectorTraits;

  /**
   *  @brief  This specialization automatically generates the trait for classes
   *          meeting specific requirements.
   *          Those requirements are:
   *          - A static integral variable @a dimension indicating the
   *            dimension of the vector and convertible to std::uint32_t.
   *          - A typedef @a ValueType of size > 0 denoting the type
   *            of the internal vector's values.
   *          - A @a at member function taking a parameter convertible from
   *            a std::uint16_t and returning a reference to the internal
   *            and corresponding @a ValueType.
   *          - A const @a at member function taking a parameter convertible
   *            from a std::uint16_t and returning the corresponding
   *            const @a ValueType.
   *
   *          This struct will then be a proxy to everything above and will
   *          be used by Transvector internally to enable some features.
   *
   *  @tparam Vec The Vector.
   */
  template <typename Vec>
  struct VectorTraits<
    Vec,
    std::enable_if_t<(Vec::dimension > 0)
                     && (sizeof(typename Vec::ValueType) > 0)>
  >
  {
    /**
     *  @brief  Typedef to the Vector's type.
     */
    using                       VectorType  = Vec;
    
    /**
     *  @brief  Typedef to the Vector members' type.
     */
    using                       ValueType   = typename Vec::ValueType;

    /**
     *  @brief  Dimension of the Vector.
     */
    static const std::uint32_t  dimension   = Vec::dimension;

    /**
     *  @brief  Proxy to the non-const version of @a VectorType::at.
     *
     *  @param[in]  v The Vector to call at on.
     *  @param[in]  i The index of the member to get.
     *
     *  @return A mutable reference to the @a ValueType at index @a i.
     */
    static constexpr ValueType& at(VectorType& v, std::uint16_t i)
    {
      return v.at(i);
    }

    /**
     *  @brief  Proxy to the const version of @a VectorType::at.
     *
     *  @param[in]  v The Vector to call at on.
     *  @param[in]  i The index of the member to get.
     *
     *  @return A const copy to the @a ValueType at index @a i.
     */
    static constexpr const ValueType at(const VectorType& v, std::uint16_t i)
    {
      return v.at(i);
    }
  };

} // namespace Transvector

#endif // TRANSVECTOR_VECTORTRAITS_HH_
