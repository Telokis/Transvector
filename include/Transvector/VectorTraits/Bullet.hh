#pragma once

#ifndef TRANSVECTOR_TRAITS_BULLET_HH_
#define TRANSVECTOR_TRAITS_BULLET_HH_

#include "Transvector/VectorTraits.hh"
#include <LinearMath/btVector3.h>

namespace Transvector
{
  template <>
  struct VectorTraits<btVector3>
  {
    using                       VectorType  = btVector3;
    using                       ValueType   = btScalar;
    static const std::uint16_t  dimension   = 3;

    static constexpr ValueType& at(VectorType& t, std::uint16_t i)
    {
      return t.m_floats[i];
    }

    static constexpr const ValueType at(const VectorType& t, std::uint16_t i)
    {
      return t.m_floats[i];
    }
  };
} // namespace Transvector

#endif // TRANSVECTOR_TRAITS_BULLET_HH_
