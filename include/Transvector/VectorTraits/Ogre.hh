#pragma once

#ifndef TRANSVECTOR_TRAITS_OGRE_HH_
#define TRANSVECTOR_TRAITS_OGRE_HH_

#include "Transvector/VectorTraits.hh"
#include <OgreVector2.h>
#include <OgreVector3.h>
#include <OgreVector4.h>

namespace Transvector
{
  template <>
  struct VectorTraits<Ogre::Vector2>
  {
    using                       VectorType  = Ogre::Vector2;
    using                       ValueType   = Ogre::Real;
    static const std::uint16_t  dimension   = 2;

    static ValueType& at(VectorType& t, std::uint16_t i)
    {
      return t[i];
    }

    static const ValueType at(const VectorType& t, std::uint16_t i)
    {
      return t[i];
    }
  };

  template <>
  struct VectorTraits<Ogre::Vector3>
  {
    using                       VectorType  = Ogre::Vector3;
    using                       ValueType   = Ogre::Real;
    static const std::uint16_t  dimension   = 3;

    static ValueType& at(VectorType& t, std::uint16_t i)
    {
      return t[i];
    }

    static const ValueType at(const VectorType& t, std::uint16_t i)
    {
      return t[i];
    }
  };

  template <>
  struct VectorTraits<Ogre::Vector4>
  {
    using                       VectorType  = Ogre::Vector4;
    using                       ValueType   = Ogre::Real;
    static const std::uint16_t  dimension   = 4;

    static ValueType& at(VectorType& t, std::uint16_t i)
    {
      return t[i];
    }

    static const ValueType at(const VectorType& t, std::uint16_t i)
    {
      return t[i];
    }
  };
} // namespace Transvector

#endif // TRANSVECTOR_TRAITS_OGRE_HH_
