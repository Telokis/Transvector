#pragma once

#ifndef TRANSVECTOR_TRAITS_SFML_HH_
#define TRANSVECTOR_TRAITS_SFML_HH_

#include "Transvector/VectorTraits.hh"
#include <SFML/System/Vector2.hpp>
#include <SFML/System/Vector3.hpp>

namespace Transvector
{
  template <typename T>
  struct VectorTraits<sf::Vector3<T>>
  {
    using                       VectorType  = sf::Vector3<T>;
    using                       ValueType   = T;
    static const std::uint16_t  dimension   = 3;

    static ValueType& at(VectorType& t, std::uint16_t i)
    {
      switch (i)
      {
        case 0:
          return t.x;
        case 1:
          return t.y;
        case 2:
          return t.z;
        default:
          throw std::out_of_range("Out of bounds");
      }
    }

    static const ValueType at(const VectorType& t, std::uint16_t i)
    {
      switch (i)
      {
        case 0:
          return t.x;
        case 1:
          return t.y;
        case 2:
          return t.z;
        default:
          throw std::out_of_range("Out of bounds");
      }
    }
  };

  template <typename T>
  struct VectorTraits<sf::Vector2<T>>
  {
    using                       VectorType  = sf::Vector2<T>;
    using                       ValueType   = T;
    static const std::uint16_t  dimension   = 2;

    static ValueType& at(VectorType& t, std::uint16_t i)
    {
      switch (i)
      {
        case 0:
          return t.x;
        case 1:
          return t.y;
        default:
          throw std::out_of_range("Out of bounds");
      }
    }

    static const ValueType at(const VectorType& t, std::uint16_t i)
    {
      switch (i)
      {
        case 0:
          return t.x;
        case 1:
          return t.y;
        default:
          throw std::out_of_range("Out of bounds");
      }
    }
  };
} // namespace Transvector

#endif // TRANSVECTOR_TRAITS_SFML_HH_
