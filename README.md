# Transvector
Small library providing a meta (Generic) Vector of N-dimension and allowing cross-library Vector conversions and operations.

[![Linux Build][travis-image]][travis-url]
[![Windows Build][appveyor-image]][appveyor-url]
[![Coverage Status][coveralls-image]][coveralls-url]

Prerequisites
------

- `CMake 3.0`
- C++14 compliant compiler

Build
------
_For now, **Transvector** only supports static linking._<br><br>
To be able to build **Transvector**, all you need is **CMake** and a **C++11 compiler**.
* Generate a project using CMake
* Linux:
    * `make` to compile **Transvector**
    * `sudo make install` to install **Transvector**
* Windows:
    * Open `Transvector.sln` in Visual Studio
    * Build the project `ALL_BUILD` in `Debug` and/or `Release` depending on your needs
    * Build the project `INSTALL` in `Debug` and/or `Release` depending on your needs
* **Transvector** is ready to use.

Linkage
------
Since this is a header-only library, there is no dll or library to be found. You only need to include the headers.<br>
**Transvector** provides a `FindTransvector.cmake` module to help the user locate what he needs.<br>
This module will try to find the installed components needed to use **Transvector**.<br>
To ensure it finds everything, the user can set the variable `TRANSVECTOR_ROOT` to the installation directory.<br>
If this variable is not set, the module will look under the following paths :
```cmake
C:/Program Files (x86)/transvector
C:/Program Files/transvector
/usr/local/
/usr/
/sw
/opt/local/
/opt/csw/
/opt/
```
And in the environment variable `TRANSVECTOR_ROOT`.

After attempting to find **Transvector**, the module will set `TRANSVECTOR_FOUND` to either `TRUE` or `FALSE`.<br>
If **Transvector** was found, this additional variable will be provided by the module:
* `TRANSVECTOR_INCLUDE_DIR`      - The include directory of Transvector

#### Minimal CMake example
Given that **Transvector** was installed in `D:/Libs/Transvector`, a minimal example to link a target `test_transvector` against **Transvector** in a CMake file would be:
```cmake
# Set the root directory of Transvector properly
SET(TRANSVECTOR_ROOT "D:/Libs/Transvector")
# Tells CMake that it can look for additional modules here
SET(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${TRANSVECTOR_ROOT}/cmake/")

# Attempts to find Transvector
FIND_PACKAGE(Transvector REQUIRED)
# If Transvector was properly found
IF (TRANSVECTOR_FOUND)
  # Adds the include directory of Transvector to the global include directories
  INCLUDE_DIRECTORIES(${TRANSVECTOR_INCLUDE_DIR})
ENDIF(TRANSVECTOR_FOUND) 
```

## Authors

* **Telokis** - *v1*

## License

[MIT](LICENSE)

[travis-image]: https://img.shields.io/travis/Ninetainedo/Transvector/master.svg?label=linux
[travis-url]: https://travis-ci.org/Ninetainedo/Transvector
[appveyor-image]: https://img.shields.io/appveyor/ci/Ninetainedo/Transvector/master.svg?label=windows
[appveyor-url]: https://ci.appveyor.com/project/Ninetainedo/Transvector
[coveralls-image]: https://img.shields.io/coveralls/Ninetainedo/Transvector/master.svg
[coveralls-url]: https://coveralls.io/r/Ninetainedo/Transvector?branch=master
