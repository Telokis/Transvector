function(prefix_list output_result input_prefix input_list)
  foreach(val IN LISTS ${input_list})
    list(APPEND FUNCTION_RESULT "${input_prefix}${val}")
  endforeach(val)
  set(${output_result} ${FUNCTION_RESULT} PARENT_SCOPE)
endfunction(prefix_list)

# Defines a macro to ease the creation of new tests
macro(create_test TestName)
  cmake_parse_arguments(TRANSVECTOR_TESTING "" "NAMESPACE;LIBRARY;PREFIX;TARGET_NAME_VAR" "FILES;INCLUDE;DEPENDENCIES" ${ARGN})

  set(FINAL_TEST_NAME ${TestName}_${TRANSVECTOR_TESTING_NAMESPACE}_Test)

  prefix_list(PREFIXED_SOURCES ${TRANSVECTOR_TESTING_PREFIX} TRANSVECTOR_TESTING_FILES)

  if (UNIX)
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")
    if (BUILD_COVERAGE_MODE AND "${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
      set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -O0") # debug, no optimisation
      set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fprofile-arcs -ftest-coverage --coverage -fno-inline -fno-inline-small-functions -fno-default-inline") # enabling coverage
    endif()
  endif()

  # message("----------------------DEBUG---------------------")
  # message("NAMESPACE: ${TRANSVECTOR_TESTING_NAMESPACE}")
  # message("LIBRARY: ${TRANSVECTOR_TESTING_LIBRARY}")
  # message("PREFIX: ${TRANSVECTOR_TESTING_PREFIX}")
  # message("FILES: ${TRANSVECTOR_TESTING_FILES}")
  # message("INCLUDE: ${TRANSVECTOR_TESTING_INCLUDE}")
  # message("DEPENDENCIES: ${TRANSVECTOR_TESTING_DEPENDENCIES}")
  # message("PREFIXED: ${PREFIXED_SOURCES}")
  # message("TARGET_NAME_VAR: ${TRANSVECTOR_TESTING_TARGET_NAME_VAR}")
  # message("------------------------------------------------")

  if (TRANSVECTOR_TESTING_TARGET_NAME_VAR)
    set(${TRANSVECTOR_TESTING_TARGET_NAME_VAR} ${FINAL_TEST_NAME})
  endif()

  add_executable(${FINAL_TEST_NAME} ${PREFIXED_SOURCES})

  set_target_properties(${FINAL_TEST_NAME} PROPERTIES
    FOLDER Tests/${TRANSVECTOR_TESTING_NAMESPACE}
  )

  target_link_libraries(
    ${FINAL_TEST_NAME}
    ${TRANSVECTOR_TESTING_LIBRARY}
    ${TRANSVECTOR_TESTING_DEPENDENCIES}
    gtest
    gtest_main
  )

  target_include_directories(${FINAL_TEST_NAME} PRIVATE ${TRANSVECTOR_TESTING_INCLUDE})
  add_test(NAME ${FINAL_TEST_NAME} COMMAND ${FINAL_TEST_NAME})
endmacro()