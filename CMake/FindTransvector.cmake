# Try to find the Transvector library
# 
# This module defines the following variables:
# 
# TRANSVECTOR_FOUND            - If Transvector was found
# TRANSVECTOR_INCLUDE_DIR      - The include directory of Transvector
# 
# This module will use the following variables:
# 
# TRANSVECTOR_ROOT - This will be used to find Transvector
# 

# Defines potential paths for finding Transvector
SET(TRANSVECTOR_FIND_PATHS
  ${TRANSVECTOR_ROOT}
  "C:/Program Files (x86)/transvector"
  "C:/Program Files/transvector"
  $ENV{TRANSVECTOR_ROOT}
  /usr/local/
  /usr/
  /sw
  /opt/local/
  /opt/csw/
  /opt/
)

if (NOT TRANSVECTOR_FIND_QUIETLY)
  message(STATUS "Looking for Transvector...")
endif ()

# Look for include folder
find_path(TRANSVECTOR_INCLUDE_DIR NAMES Transvector/GenericVector.hh
  HINTS
    ${TRANSVECTOR_FIND_PATHS}
  PATH_SUFFIXES include)

# If at least one library was found
if (TRANSVECTOR_INCLUDE_DIR)
  # Mark as found
  SET(TRANSVECTOR_FOUND TRUE)
else()
  # Transvector was not found
  SET(TRANSVECTOR_FOUND FALSE)
endif()

# Don't show variables to user
mark_as_advanced(
  TRANSVECTOR_INCLUDE_DIR
)

if (TRANSVECTOR_FOUND)
  message(STATUS "-- Found Transvector : ${TRANSVECTOR_INCLUDE_DIR}")
else()
  if(TRANSVECTOR_FIND_REQUIRED)
    # Fatal error
    message(FATAL_ERROR "Could NOT find Transvector")
  elseif(NOT TRANSVECTOR_FIND_QUIETLY)
    # Error, but continue
    message("Could NOT find Transvector")
  endif()
endif()
